<?php

namespace StayOut\Db;

use Propel\Runtime\Propel;
use Propel\Runtime\Connection\ConnectionManagerSingle;

class Connection
{
	/**
	 * @var string $dsn
	 */
	private $dsn;

	/**
	 * @var string $user
	 */
	private $user;

	/**
	 * @var string $password
	 */
	private $password;

	public function __construct()
	{
		$this->dsn = 'pgsql:host='.getenv('DB_HOST').';port='.getenv('DB_PORT').';dbname='.getenv('DB_BASE');
		$this->user = getenv('DB_USER');
		!empty(getenv('DB_PASS')) ? $this->password = getenv('DB_PASS') : $this->password = '';
	}

	/**
	 * Connect ORM
	 */
	public function get()
	{
		$serviceContainer = Propel::getServiceContainer();
		$serviceContainer->checkVersion('2.0.0-dev');
		$serviceContainer->setAdapterClass('default', 'pgsql');

		$manager = new ConnectionManagerSingle();
		$manager->setConfiguration([
			'dsn' => $this->dsn,
			'user' => $this->user,
			'password' => $this->password,
			'settings' => [
				'charset' => 'utf8',
				'queries' => []
			],
			'classname' => '\\Propel\\Runtime\\Connection\\ConnectionWrapper',
			'model_paths' => [
				0 => 'src',
				1 => 'vendor',
			]
		]);
		$manager->setName('default');

		$serviceContainer->setConnectionManager('default', $manager);
		$serviceContainer->setDefaultDatasource('default');
	}
}
