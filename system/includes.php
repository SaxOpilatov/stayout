<?php

namespace StayOut;

use GraphQL\Type\Definition\ObjectType;
use StayOut\Exception\IncludeException;
use StayOut\Object\Route;

class Includes
{
	private static $main = [
		'/db/db.php',
        '/router.php',
        '/graphql.php',
        '/assign.php',
        '/input.php',
        '/mailer.php',
        '/strings.php',
		'/xxtea.php'
	];

	private static $enums = [
	    'HttpMethods',
        'Mails',
		'UserFields',
		'Langs',
		'UserAccess'
    ];

	private static $modules = [
	    'Auth'
    ];

    /**
     * @param string $file
     * @param bool $internal
     * @param bool|null $withError
     * @throws \Exception
     */
	private static function inc  (string $file, bool $internal, ?bool $withError = false) : void
	{
		$internal ? $path = __DIR__ : $path = __DIR__ . '/../app';
		if (file_exists($path . $file))
		{
			require_once $path . $file;
		} else {
		    if ($withError)
            {
                throw new IncludeException('Can\'t connect file: ' . $file, 3);
            }
        }
	}

    /**
     * @throws \Exception
     */
	public static function Main () : void
	{
		if (is_countable(self::$main))
		{
			foreach (self::$main as $file)
			{
				self::inc($file, true);
			}
		}
	}

    /**
     * @throws \Exception
     */
	public static function Enums () : void
    {
        if (is_countable(self::$enums))
        {
            foreach (self::$enums as $file)
            {
                self::inc('/objects/enums/enum' . $file . '.php', true);
            }
        }
    }

    /**
     * @param Route $route
     * @return bool
     * @throws \Exception
     */
    public static function Controller (Route $route) : bool
    {
        $routeParts = explode("\\", $route->action);
        list($_, $_, $controller, $action) = $routeParts;

        $file = '/Controllers/' . mb_strtolower($controller) . '/' . $action . '.php';

        try {
            self::inc($file, false, true);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @throws \Exception
     */
    public static function Modules () : void
    {
        if (is_countable(self::$modules))
        {
            foreach (self::$modules as $module)
            {
                self::inc('/objects/traits/' . $module . '.php', true);
            }
        }
    }
}
