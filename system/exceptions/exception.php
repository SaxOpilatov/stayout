<?php

namespace StayOut\Exception;

require_once __DIR__ . '/AssignException.php';
require_once __DIR__ . '/ExecuteException.php';
require_once __DIR__ . '/IncludeException.php';
require_once __DIR__ . '/ModuleException.php';

class Main extends \Exception
{
    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'message', 'string', 'code', 'file', 'line', 'trace'
        ];
    }

    public function __wakeup()
    {

    }
}