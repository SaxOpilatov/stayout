<?php

namespace StayOut;

use StayOut\DB\EmailTemplatesQuery;
use StayOut\Exception\ExecuteException;
use StayOut\Exception\IncludeException;
use StayOut\Object\Enum\Mails;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer
{
    /**
     * @param Mails $enum
     * @return string
     * @throws IncludeException
     */
    private static function getTemplate (Mails $enum) : string
    {
        $modelTemplate = new EmailTemplatesQuery();
        $template = $modelTemplate::create()
            ->filterByCode($enum->getValue())
            ->findOne();

        if (empty($template))
        {
            throw new IncludeException('Can\t send email', 16);
        }

        return $template->getTemplate();
    }

    /**
     * @param string $address
     * @param string $subject
     * @param string $body
     * @return bool
     */
    private static function send (string $address, string $subject, string $body) : bool
    {
        try {
            $mailer = new PHPMailer(true);
            $mailer->isSMTP();
            $mailer->isHTML(true);
            $mailer->CharSet = "utf-8";
            $mailer->SMTPAuth = true;
            $mailer->SMTPSecure = 'ssl';
            $mailer->Host = getenv('SMTP_HOST');
            $mailer->Username = getenv('SMTP_USER');
            $mailer->Password = getenv('SMTP_PASS');
            $mailer->Port = 465;
            $mailer->SMTPDebug = 0;

            $mailer->setFrom(getenv('SMTP_FROM'), getenv('SMTP_NAME'), 0);
            $mailer->addAddress($address);

            $mailer->Subject = $subject;
            $mailer->msgHTML($body);

            $mailer->send();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param string $email
     * @param string $hash
     * @param int $code
     * @return bool
     * @throws ExecuteException
     * @throws IncludeException
     */
    public static function signupActivationCode (string $email, string $hash, int $code) : bool
    {
        try {
            $template = self::getTemplate(Mails::SIGNUP_ACTIVATION_CODE());

            $subject = 'Код активации';
            $body = str_replace(['{{ code }}', '{{ hash }}'], [(string)$code, (string)$hash], $template);

            if (self::send($email, $subject, $body))
            {
                return true;
            } else {
                throw new ExecuteException('Can\t send email', 18);
            }
        } catch (ExecuteException $e) {
            throw new ExecuteException('Can\t send email', 17);
        }
    }

	/**
	 * @param string $email
	 * @param string $hash
	 * @param int $code
	 * @return bool
	 * @throws ExecuteException
	 * @throws IncludeException
	 */
    public static function passwdRestoreCode (string $email, string $hash, int $code) : bool
	{
		try {
			$template = self::getTemplate(Mails::PASSWD_RESTORE_CODE());

			$subject = 'Код';
			$body = str_replace(['{{ code }}', '{{ hash }}'], [(string)$code, (string)$hash], $template);

			if (self::send($email, $subject, $body))
			{
				return true;
			} else {
				throw new ExecuteException('Can\t send email', 18);
			}
		} catch (ExecuteException $e) {
			throw new ExecuteException('Can\t send email', 17);
		}
	}
}