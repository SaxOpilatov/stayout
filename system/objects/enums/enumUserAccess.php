<?php

namespace StayOut\Object\Enum;

use Spatie\Enum\Enum;

/**
 * Class UserAccess
 * @package StayOut\Object\Enum
 * ============================
 * @method static self admin()
 */
class UserAccess extends Enum
{

}