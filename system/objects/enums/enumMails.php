<?php

namespace StayOut\Object\Enum;

use Spatie\Enum\Enum;

/**
 * Class Mails
 * @package StayOut\Object\Enum
 * ============================
 * @method static self SIGNUP_ACTIVATION_CODE()
 * @method static self PASSWD_RESTORE_CODE()
 */
class Mails extends Enum
{

}