<?php

namespace StayOut\Object\Enum;

use Spatie\Enum\Enum;

/**
 * Class Langs
 * @package StayOut\Object\Enum
 * ============================
 * @method static self ru()
 */
class Langs extends Enum
{

}