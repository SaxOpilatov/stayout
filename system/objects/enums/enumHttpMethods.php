<?php

namespace StayOut\Object\Enum;

use Spatie\Enum\Enum;

/**
 * Class HttpMethods
 * @package StayOut\Object\Enum
 * ============================
 * @method static self any()
 * @method static self get()
 * @method static self post()
 * @method static self put()
 * @method static self delete()
 */
class HttpMethods extends Enum
{

}