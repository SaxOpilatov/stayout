<?php

namespace StayOut\Object\Enum;

use Spatie\Enum\Enum;

/**
 * Class UserFields
 * @package StayOut\Object\Enum
 * ============================
 * @method static self country()
 * @method static self city()
 * @method static self full_name()
 * @method static self image()
 * @method static self sex()
 * @method static self birth_date()
 * @method static self about()
 */
class UserFields extends Enum
{

}