<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\ObjectType;
use StayOut\Object\Types;

class DataCountryType extends ObjectType
{
	public function __construct ()
	{
		$config = [
			'name' => 'DataCountry',
			'description' => 'Countries list',
			'fields' => function() {
				return [
					'id' => Types::id(),
					'code' => Types::string(),
					'name' => Types::string()
				];
			},
			'interfaces' => [
				Types::node()
			]
		];

		parent::__construct($config);
	}
}