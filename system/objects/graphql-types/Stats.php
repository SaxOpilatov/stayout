<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\ObjectType;
use StayOut\Object\Types;

class StatsType extends ObjectType
{
	public function __construct ()
	{
		$config = [
			'name' => 'Stats',
			'description' => 'Stats',
			'fields' => function() {
				return [
					'users' => Types::int(),
					'usersLast7days' => Types::int(),
					'usersLast1month' => Types::int(),
					'usersLast3months' => Types::int(),
					'usersLast6months' => Types::int(),
					'places' => Types::int(),
					'placesLast7days' => Types::int(),
					'placesLast1month' => Types::int(),
					'placesLast3months' => Types::int(),
					'placesLast6months' => Types::int(),
					'events' => Types::int(),
					'eventsLast7days' => Types::int(),
					'eventsLast1month' => Types::int(),
					'eventsLast3months' => Types::int(),
					'eventsLast6months' => Types::int()
				];
			},
			'interfaces' => [
				Types::node()
			]
		];

		parent::__construct($config);
	}
}
