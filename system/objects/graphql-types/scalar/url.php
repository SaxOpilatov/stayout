<?php

namespace StayOut\Object\GraphqlTypes\Scalar;

use GraphQL\Type\Definition\CustomScalarType;
use GraphQL\Utils\Utils;
use GraphQL\Error\Error;

class UrlType
{
	/**
	 * @param mixed $value
	 * @return string
	 * @throws Error
	 */
	public function serialize ($value) : string
	{
		return self::parseValue($value);
	}

	/**
	 * @param mixed $value
	 * @return string
	 * @throws Error
	 */
	public function parseValue ($value) : string
	{
		if (!is_string($value) || !filter_var($value, FILTER_VALIDATE_URL))
		{
			throw new Error("Cannot represent value as URL: " . Utils::printSafe($value));
		}

		return $value;
	}
}