<?php

namespace StayOut\Object\GraphqlTypes\Scalar;

use GraphQL\Type\Definition\CustomScalarType;
use GraphQL\Utils\Utils;
use GraphQL\Error\Error;

class EmailType
{
	public static function create()
	{
		return new CustomScalarType([
			'name' => 'Email',
			'serialize' => [__CLASS__, 'serialize'],
			'parseValue' => [__CLASS__, 'parseValue'],
			'parseLiteral' => [__CLASS__, 'parseLiteral'],
		]);
	}

	public static function serialize ($value)
	{
//		return self::parseValue($value);
		return $value;
	}

	public static function parseValue ($value)
	{
		if (!filter_var($value, FILTER_VALIDATE_EMAIL))
		{
			throw new \UnexpectedValueException("Cannot represent value as email: " . Utils::printSafe($value));
		}

		return $value;
	}
}