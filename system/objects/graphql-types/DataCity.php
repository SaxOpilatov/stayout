<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\ObjectType;
use StayOut\Object\Types;

class DataCityType extends ObjectType
{
	public function __construct ()
	{
		$config = [
			'name' => 'DataCity',
			'description' => 'Cities list',
			'fields' => function() {
				return [
					'id' => Types::id(),
					'country' => [
						'type' => Types::dataCountry(),
						'description' => 'Selected country'
					],
					'name' => Types::string()
				];
			},
			'interfaces' => [
				Types::node()
			]
		];

		parent::__construct($config);
	}
}