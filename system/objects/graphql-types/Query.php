<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use StayOut\Object\GraphqlTypes\Traits\QueryStatsGraphQL;
use StayOut\Object\Types;
use StayOut\Object\GraphqlTypes\Traits\QueryAirGraphQL;
use StayOut\Object\GraphqlTypes\Traits\QueryDataGraphQL;
use StayOut\Object\GraphqlTypes\Traits\QueryUsersGraphQL;

require_once __DIR__ . '/traits/qData.php';
require_once __DIR__ . '/traits/qAir.php';
require_once __DIR__ . '/traits/qUsers.php';
require_once __DIR__ . '/traits/qStats.php';

class QueryType extends ObjectType
{
	use QueryDataGraphQL;
	use QueryAirGraphQL;
	use QueryUsersGraphQL;
	use QueryStatsGraphQL;

	public function __construct()
	{
		$config = [
			'name' => 'Query',
			'fields' => [
				'hello' => Type::string(),

				// Countries
				'dataCountry' => [
					'type' => Types::dataCountry(),
					'description' => 'Get country',
					'args' => [
						'id' => Types::nonNull(Types::id())
					]
				],
				'dataCountries' => [
					'type' => Types::listOf(Types::dataCountry()),
					'description' => 'Get countries',
					'args' => [
						'name' => Types::string()
					]
				],

				// Cities
				'dataCity' => [
					'type' => Types::dataCity(),
					'description' => 'Get city',
					'args' => [
						'id' => Types::nonNull(Types::id())
					]
				],
				'dataCities' => [
					'type' => Types::listOf(Types::dataCity()),
					'description' => 'Get cities',
					'args' => [
						'name' => Types::string(),
						'country' => Types::id()
					]
				],

				// Air
				'airImage' => [
					'type' => Types::airImage(),
					'description' => 'Get image',
					'args' => [
						'path' => Types::nonNull(Types::string())
					]
				],

				// Users
				'profile' => [
					'type' => Types::user(),
					'description' => 'Get profile'
				],

				'user' => [
					'type' => Types::user(),
					'description' => 'Returns user by params',
					'args' => [
						'id' => Types::id()
					]
				],
				'users' => [
					'type' => Types::listOf(Types::user()),
					'description' => 'Returns list of users by params'
				],
				'adminLinks' => [
					'type' => Types::listOf(Types::string())
				],

				// Stats
				'stats' => [
					'type' => Types::stats(),
					'description' => 'Returns short stats'
				]
			],
			'resolveField' => function($val, $args, $context, ResolveInfo $info) {
				return $this->{$info->fieldName}($val, $args, $context, $info);
			}
		];

		parent::__construct($config);
	}

	public function hello ()
	{
		return 'Your graphql-php endpoint is ready! Use GraphiQL to browse API';
	}
}
