<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\ObjectType;
use StayOut\Object\Types;

class UserType extends ObjectType
{
	public function __construct ()
	{
		$config = [
			'name' => 'User',
			'description' => 'Our users',
			'fields' => function() {
				return [
					'id' => Types::id(),
					'login' => Types::string(),
					'email' => Types::email(),
					'country' => [
						'type' => Types::dataCountry(),
						'description' => 'Selected country'
					],
					'city' => [
						'type' => Types::dataCity(),
						'description' => 'Selected city'
					],
					'full_name' => Types::string(),
					'image' => [
						'type' => Types::airImage(),
						'description' => 'Selected image'
					],
					'sex' => Types::int(),
					'birth_date' => Types::string(),
					'about' => Types::string()
				];
			},
			'interfaces' => [
				Types::node()
			]
		];

		parent::__construct($config);
	}
}