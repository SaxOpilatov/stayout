<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use StayOut\Object\GraphqlTypes\Traits\MutationUsersGraphQL;
use StayOut\Object\Types;

require_once __DIR__ . '/traits/mUsers.php';

class MutationType extends ObjectType
{
	use MutationUsersGraphQL;

	public function __construct()
	{
		$config = [
			'name' => 'Mutation',
			'fields' => [
				'yo' => [
					'type' => Types::string(),
					'args' => [
						'message' => Types::string()
					],
					'resolve' => function($val, $args, $context, ResolveInfo $info) {
						return $args['message'];
					}
				],

				// Users
				'profile' => [
					'type' => Types::user(),
					'description' => 'Edit profile',
					'args' => [
						'login' => Types::string(),
						'country' => Types::id(),
						'city' => Types::id(),
						'full_name' => Types::string(),
						'image' => Types::string(),
						'sex' => Types::int(),
						'birth_date' => Types::string(),
						'about' => Types::string()
					]
				]
			],
			'resolveField' => function($val, $args, $context, ResolveInfo $info) {
				return $this->{$info->fieldName}($val, $args, $context, $info);
			}
		];

		parent::__construct($config);
	}
}