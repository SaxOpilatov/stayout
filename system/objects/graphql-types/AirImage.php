<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\ObjectType;
use StayOut\Object\Types;

class AirImageType extends ObjectType
{
	public function __construct ()
	{
		$config = [
			'name' => 'AirImage',
			'description' => 'Image without db',
			'fields' => function() {
				return [
					'path' => Types::string(),
					'origin' => Types::string(),
					'w50_square' => Types::string(),
					'w50_origin' => Types::string(),
					'w100_square' => Types::string(),
					'w100_origin' => Types::string(),
					'w200_square' => Types::string(),
					'w200_origin' => Types::string(),
					'w500_square' => Types::string(),
					'w500_origin' => Types::string(),
					'w1000_square' => Types::string(),
					'w1000_origin' => Types::string()
				];
			}
		];

		parent::__construct($config);
	}
}