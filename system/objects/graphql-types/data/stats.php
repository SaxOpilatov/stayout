<?php

namespace StayOut\Object\GraphqlTypes\Data;

use GraphQL\Utils\Utils;

class Stats
{
	public $users;
	public $usersLast7days;
	public $usersLast1month;
	public $usersLast3months;
	public $usersLast6months;
	public $places;
	public $placesLast7days;
	public $placesLast1month;
	public $placesLast3months;
	public $placesLast6months;
	public $events;
	public $eventsLast7days;
	public $eventsLast1month;
	public $eventsLast3months;
	public $eventsLast6months;

	public function __construct(array $data)
	{
		Utils::assign($this, $data);
	}
}
