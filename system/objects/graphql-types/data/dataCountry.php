<?php

namespace StayOut\Object\GraphqlTypes\Data;

use GraphQL\Utils\Utils;

class DataCountry
{
	public $id;
	public $code;
	public $name;

	public function __construct(array $data)
	{
		Utils::assign($this, $data);
	}
}