<?php

namespace StayOut\Object\GraphqlTypes\Data;

use GraphQL\Utils\Utils;

class User
{
	public $id;
	public $login;
	public $email;
	public $country;
	public $city;
	public $full_name;
	public $image;
	public $sex;
	public $birth_date;
	public $about;

	public function __construct(array $data)
	{
		Utils::assign($this, $data);
	}
}