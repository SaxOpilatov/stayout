<?php

namespace StayOut\Object\GraphqlTypes\Data;

use GraphQL\Utils\Utils;

class AirImage
{
	public $path;
	public $origin;
	public $w50_square;
	public $w50_origin;
	public $w100_square;
	public $w100_origin;
	public $w200_square;
	public $w200_origin;
	public $w500_square;
	public $w500_origin;
	public $w1000_square;
	public $w1000_origin;

	public function __construct(array $data)
	{
		if (!empty($data['path']))
		{
			if (empty($data['origin']))
			{
				$url = getenv('APP_CDNURL') . '/' . $data['path'];

				$data['origin'] = $url . '.png';
				$data['w50_square'] = $url . '_50_square.png';
				$data['w50_origin'] = $url . '_50_origin.png';
				$data['w100_square'] = $url . '_100_square.png';
				$data['w100_origin'] = $url . '_100_origin.png';
				$data['w200_square'] = $url . '_200_square.png';
				$data['w200_origin'] = $url . '_200_origin.png';
				$data['w500_square'] = $url . '_500_square.png';
				$data['w500_origin'] = $url . '_500_origin.png';
				$data['w1000_square'] = $url . '_1000_square.png';
				$data['w1000_origin'] = $url . '_1000_origin.png';
			}
		} else {
			$data['origin'] = null;
			$data['w50_square'] = null;
			$data['w50_origin'] = null;
			$data['w100_square'] = null;
			$data['w100_origin'] = null;
			$data['w200_square'] = null;
			$data['w200_origin'] = null;
			$data['w500_square'] = null;
			$data['w500_origin'] = null;
			$data['w1000_square'] = null;
			$data['w1000_origin'] = null;
		}

		Utils::assign($this, $data);
	}
}