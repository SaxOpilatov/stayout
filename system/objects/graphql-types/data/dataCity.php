<?php

namespace StayOut\Object\GraphqlTypes\Data;

use GraphQL\Utils\Utils;

class DataCity
{
	public $id;
	public $country;
	public $name;

	public function __construct(array $data)
	{
		Utils::assign($this, $data);
	}
}