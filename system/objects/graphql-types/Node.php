<?php

namespace StayOut\Object\GraphqlTypes;

use GraphQL\Type\Definition\InterfaceType;
use StayOut\Object\GraphqlTypes\Data\User;
use StayOut\Object\Types;

class NodeType extends InterfaceType
{
	public function __construct()
	{
		$config = [
			'name' => 'Node',
			'fields' => [
				'id' => Types::id()
			],
			'resolveType' => [$this, 'resolveNodeType']
		];

		parent::__construct($config);
	}

	public function resolveNodeType ($object)
	{
		if ($object instanceof User)
		{
			return Types::user();
		} else {
			// temp
			return Types::user();
		}
	}
}