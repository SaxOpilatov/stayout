<?php

namespace StayOut\Object\GraphqlTypes\Traits;

use StayOut\Controller\User\AdminLinksAction;
use StayOut\Controller\User\ProfileAction;
use StayOut\GraphQL;
use StayOut\Object\Arch;
use StayOut\Object\GraphqlTypes\Data\User;

trait QueryUsersGraphQL
{
	public function profile ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(ProfileAction::class, $rootValue, $args, $arch);
	}

	public function user ($rootValue, $args, Arch $arch)
	{
		return new User([
			'id' => '1'
		]);
	}

	public function users ($rootValue, $args, Arch $arch)
	{
		return [
			'1' => new User([
				'id' => '1'
			]),

			'2' => new User([
				'id' => '2'
			])
		];
	}

	public function adminLinks ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(AdminLinksAction::class, $rootValue, $args, $arch);
	}
}
