<?php

namespace StayOut\Object\GraphqlTypes\Traits;

use StayOut\Controller\User\ProfileEditAction;
use StayOut\GraphQL;
use StayOut\Object\Arch;

trait MutationUsersGraphQL
{
	public function profile ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(ProfileEditAction::class, $rootValue, $args, $arch);
	}
}