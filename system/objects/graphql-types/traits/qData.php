<?php

namespace StayOut\Object\GraphqlTypes\Traits;

use StayOut\Controller\Data\CitiesAction;
use StayOut\Controller\Data\CityAction;
use StayOut\Controller\Data\CountriesAction;
use StayOut\Controller\Data\CountryAction;
use StayOut\GraphQL;
use StayOut\Object\Arch;

trait QueryDataGraphQL
{
	public function dataCountry ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(CountryAction::class, $rootValue, $args, $arch);
	}

	public function dataCountries ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(CountriesAction::class, $rootValue, $args, $arch);
	}

	public function dataCity ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(CityAction::class, $rootValue, $args, $arch);
	}

	public function dataCities ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(CitiesAction::class, $rootValue, $args, $arch);
	}
}