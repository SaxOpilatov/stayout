<?php

namespace StayOut\Object\GraphqlTypes\Traits;

use StayOut\Controller\Air\ImageAction;
use StayOut\GraphQL;
use StayOut\Object\Arch;

trait QueryAirGraphQL
{
	public function airImage ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(ImageAction::class, $rootValue, $args, $arch);
	}
}