<?php

namespace StayOut\Object\GraphqlTypes\Traits;

use StayOut\Controller\Stats\StatsAction;
use StayOut\GraphQL;
use StayOut\Object\Arch;

trait QueryStatsGraphQL
{
	public function stats ($rootValue, $args, Arch $arch)
	{
		return GraphQL::execute(StatsAction::class, $rootValue, $args, $arch);
	}
}