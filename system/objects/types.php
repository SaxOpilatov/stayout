<?php

namespace StayOut\Object;

use GraphQL\Type\Definition\IDType;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\StringType;
use GraphQL\Type\Definition\Type;
use StayOut\Object\GraphqlTypes\AirImageType;
use StayOut\Object\GraphqlTypes\DataCityType;
use StayOut\Object\GraphqlTypes\DataCountryType;
use StayOut\Object\GraphqlTypes\MutationType;
use StayOut\Object\GraphqlTypes\NodeType;
use StayOut\Object\GraphqlTypes\QueryType;
use StayOut\Object\GraphqlTypes\Scalar\EmailType;
use StayOut\Object\GraphqlTypes\Scalar\UrlType;
use StayOut\Object\GraphqlTypes\StatsType;
use StayOut\Object\GraphqlTypes\UserType;

// Object types
require_once __DIR__ . '/graphql-types/Query.php';
require_once __DIR__ . '/graphql-types/Mutation.php';
require_once __DIR__ . '/graphql-types/User.php';
require_once __DIR__ . '/graphql-types/DataCountry.php';
require_once __DIR__ . '/graphql-types/DataCity.php';
require_once __DIR__ . '/graphql-types/AirImage.php';
require_once __DIR__ . '/graphql-types/Stats.php';
require_once __DIR__ . '/graphql-types/Node.php';

// Data
require_once __DIR__ . '/graphql-types/data/user.php';
require_once __DIR__ . '/graphql-types/data/dataCountry.php';
require_once __DIR__ . '/graphql-types/data/dataCity.php';
require_once __DIR__ . '/graphql-types/data/airImage.php';
require_once __DIR__ . '/graphql-types/data/stats.php';

// Custom Scalar types
require_once __DIR__ . '/graphql-types/scalar/url.php';
require_once __DIR__ . '/graphql-types/scalar/email.php';

class Types
{
	// Object types
	private static $query;
	private static $mutation;
	private static $user;
	private static $stats;
	private static $dataCountry;
	private static $dataCity;
	private static $airImage;

	// Interface types
	private static $node;

	// Custom Scalar types
	private static $emailType;
	private static $urlType;

	/**
	 * @return QueryType
	 */
	public static function query () : QueryType
	{
		return self::$query ?: (self::$query = new QueryType());
	}

	/**
	 * @return MutationType
	 */
	public static function mutation () : MutationType
	{
		return self::$mutation ?: (self::$mutation = new MutationType());
	}

	/**
	 * @return UserType
	 */
	public static function user () : UserType
	{
		return self::$user ?: (self::$user = new UserType());
	}

	/**
	 * @return StatsType
	 */
	public static function stats () : StatsType
	{
		return self::$stats ?: (self::$stats = new StatsType());
	}

	/**
	 * @return DataCountryType
	 */
	public static function dataCountry () : DataCountryType
	{
		return self::$dataCountry ?: (self::$dataCountry = new DataCountryType());
	}

	/**
	 * @return DataCityType
	 */
	public static function dataCity () : DataCityType
	{
		return self::$dataCity ?: (self::$dataCity = new DataCityType());
	}

	/**
	 * @return AirImageType
	 */
	public static function airImage () : AirImageType
	{
		return self::$airImage ?: (self::$airImage = new AirImageType());
	}

	/**
	 * @return NodeType
	 */
	public static function node () : NodeType
	{
		return self::$node ?: (self::$node = new NodeType());
	}

	/**
	 * @return IDType
	 */
	public static function id () : IDType
	{
		return Type::id();
	}

	/**
	 * @return IntType
	 */
	public static function int () : IntType
	{
		return Type::int();
	}

	/**
	 * @return StringType
	 */
	public static function string () : StringType
	{
		return Type::string();
	}

	/**
	 * @return \GraphQL\Type\Definition\CustomScalarType
	 */
	public static function email ()
	{
		return self::$emailType ?: (self::$emailType = EmailType::create());
	}

	/**
	 * @return UrlType
	 */
	public static function url () : UrlType
	{
		return self::$urlType ?: (self::$urlType = new UrlType());
	}

	/**
	 * @param $type
	 * @return ListOfType
	 */
	public static function listOf ($type) : ListOfType
	{
		return new ListOfType($type);
	}

	/**
	 * @param $type
	 * @return NonNull
	 */
	public static function nonNull ($type) : NonNull
	{
		return new NonNull($type);
	}
}