<?php

namespace StayOut\Object;

use StayOut\Object\Enum\HttpMethods;

class Route
{
	/**
	 * @var HttpMethods $method
	 */
	public $method;

	/**
	 * @var string $url
	 */
	public $url;

	/**
	 * @var string $action
	 */
	public $action;

	/**
	 * Route constructor.
	 * @param HttpMethods $method
	 * @param string $url
	 * @param string $action
	 */
	public function __construct(HttpMethods $method, string $url, string $action)
	{
		$this->method = $method;
		$this->url = mb_strtolower($url);
		$this->action = $action;
	}
}
