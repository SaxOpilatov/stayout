<?php

namespace StayOut\Object;

class Auth
{
    /**
     * @var string $accessToken
     */
    public $accessToken;

    /**
     * @var string $userHash
     */
    public $userHash;

    /**
     * Auth constructor.
     * @param string $userHash
     * @param string $accessToken
     */
    public function __construct(string $userHash, string $accessToken)
    {
        $this->userHash = $userHash;
        $this->accessToken = $accessToken;
    }
}