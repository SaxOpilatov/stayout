<?php

namespace StayOut\Object\Abstracts;

use StayOut\Object\Arch;

abstract class ControllerGraphQL
{
	/**
	 * @var Arch $arch
	 */
	protected $arch;
	protected $rootValue;
	protected $args;

	public function __construct(Arch $arch, $args, $rootValue)
	{
		$this->arch = $arch;
		$this->args = $args;
		$this->rootValue = $rootValue;
	}

	abstract public function index ();
}