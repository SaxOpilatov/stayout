<?php

namespace StayOut\Object\Abstracts;

use StayOut\Object\Arch;

abstract class Controller
{
    /**
     * @var Arch $arch
     */
    protected $arch;

    abstract public function index ();

    /**
     * Controller constructor.
     * @param Arch $arch
     */
    public function __construct(Arch $arch)
    {
        $this->arch = $arch;
    }
}