<?php

namespace StayOut\Module;

use Propel\Runtime\ActiveQuery\Criteria;
use Ramsey\Uuid\Uuid;
use StayOut\DB\Users;
use StayOut\DB\UsersAccessQuery;
use StayOut\DB\UsersQuery;
use Respect\Validation\Validator as v;
use StayOut\DB\UsersRestore;
use StayOut\DB\UsersRestoreQuery;
use StayOut\DB\UsersTokens;
use StayOut\DB\UsersTokensQuery;
use StayOut\Exception\ExecuteException;
use StayOut\Mailer;
use StayOut\Object\Arch;
use StayOut\Object\Auth as AuthObj;
use StayOut\Object\Enum\UserAccess;
use StayOut\Xxtea;

require_once __DIR__ . '/../auth.php';

class Auth
{
    /**
     * @var string $nameHeadersAccessToken
     */
    private $nameHeadersAccessToken = 'X-Access-Token';

    /**
     * @var string $nameHeadersUserHash
     */
    private $nameHeadersUserHash = 'X-User-Hash';

	/**
	 * @var string $nameHeaderUserAdmin
	 */
    private $nameHeaderUserAdmin = 'X-Admin-Hash';

    /**
     * @var Arch $arch
     */
    private $arch;

    /**
     * Auth constructor.
     * @param Arch $arch
     */
    public function __construct(Arch $arch)
    {
        $this->arch = $arch;
    }

    /**
     * @throws \Exception
     */
    public function signup ()
    {
        $headers = $this->arch->input->headers();

        if (!empty($headers[$this->nameHeadersUserHash]) || !empty($headers[$this->nameHeadersAccessToken]))
        {
            $this->arch->assign->error(9, 'This method don\'t allow headers');
            $this->arch->assign->stop();
        }

        // input
        $input = new \stdClass();
        $input->login = $this->arch->input->input('login', FILTER_SANITIZE_STRIPPED);
        $input->email = $this->arch->input->input('email', FILTER_VALIDATE_EMAIL);
        $input->password = $this->arch->input->input('password', FILTER_SANITIZE_STRIPPED);

        /**
         * Validate
         */
        // login
        if (!v::stringType()->notEmpty()->validate($input->login))
        {
            $this->arch->assign->error(10, 'Login is empty', 'login');
        }

        if (
			(v::stringType()->notEmpty()->validate($input->login) && !v::alnum('._')->noWhitespace()->validate($input->login)) ||
			(v::stringType()->notEmpty()->validate($input->login) && !v::stringType()->length(5,21)->validate($input->login))
		)
        {
            $this->arch->assign->error(11, 'Login can contain only characters, numbers, . and _. Min length is 5, max length is 21', 'login');
        }

        $input->login = mb_strtolower($input->login);

        // email
        if (!v::stringType()->notEmpty()->validate($input->email))
        {
            $this->arch->assign->error(12, 'Email is empty', 'email');
        }

        if (v::stringType()->notEmpty()->validate($input->email) && !v::email()->validate($input->email))
        {
            $this->arch->assign->error(13, 'Email is incorrect', 'email');
        }

        $input->email = mb_strtolower($input->email);

        // password
        if (!v::stringType()->notEmpty()->validate($input->password))
        {
            $this->arch->assign->error(12, 'Password is empty', 'password');
        }

        if (v::stringType()->notEmpty()->validate($input->password) && !v::stringType()->length(8,null)->validate($input->password))
        {
            $this->arch->assign->error(13, 'Password is too short, minimal length is 8 characters', 'password');
        }

        $assign = $this->arch->assign->get();
        if (!empty($assign['errors']))
        {
            $this->arch->assign->stop();
        }

        /**
         * Checks
         */
        $modelUsers = new UsersQuery();

        // email
        $isEmailExists = $modelUsers::create()
            ->filterByEmail($input->email)
            ->count();

        if ($isEmailExists > 0)
        {
            $this->arch->assign->error(14, 'This email is already exist', 'email');
        }

        // login
        $isLoginExists = $modelUsers::create()
            ->filterByLogin($input->login)
            ->count();

        if ($isLoginExists > 0)
        {
            $this->arch->assign->error(15, 'This login is already exist', 'login');
        }

        $assign = $this->arch->assign->get();
        if (!empty($assign['errors']))
        {
            $this->arch->assign->stop();
        }

        /**
         * Add user
         */
        $userHashId = Uuid::uuid5(Uuid::NAMESPACE_DNS, time() . '_' . mt_rand(1000, 9999999999) . '_users.stayout.club');
        $user = new Users();
        $user->setHashId($userHashId->toString());
        $user->setEmail($input->email);
        $user->setLogin($input->login);
        $user->setPassword($this->generatePasswordHash($input->password));
        $user->setIsActivated(false);
        $user->setActivateCode(mt_rand(100000, 999999));
        $user->setIsDeleted(false);
        $user->save();

        /**
         * Send email
         */
        Mailer::signupActivationCode($user->getEmail(), $userHashId->toString(), $user->getActivateCode());

        /**
         * Output
         */
        $this->arch->assign->data('verify_hash', $userHashId->toString());
    }

    /**
     * @throws \Exception
     */
    public function verify ()
    {
        $headers = $this->arch->input->headers();

        if (!empty($headers[$this->nameHeadersUserHash]) || !empty($headers[$this->nameHeadersAccessToken]))
        {
            $this->arch->assign->error(9, 'This method don\'t allow headers');
            $this->arch->assign->stop();
        }

        // input
        $input = new \stdClass();
        $input->verify_id = $this->arch->input->input('verify_hash', FILTER_SANITIZE_STRIPPED);
        $input->verify_code = $this->arch->input->input('verify_code', FILTER_VALIDATE_INT);

        /**
         * Validate
         */
        // user hash id
        if (!v::stringType()->notEmpty()->validate($input->verify_id))
        {
            $this->arch->assign->error(16, 'Verify hash is empty', 'verify_id');
        }

        // code
        if (!v::intType()->notEmpty()->validate($input->verify_code))
		{
			$this->arch->assign->error(17, 'Code is empty', 'verify_code');
		}

        $assign = $this->arch->assign->get();
        if (!empty($assign['errors']))
        {
            $this->arch->assign->stop();
        }

        /**
         * Checks
         */
        $modelUsers = new UsersQuery();
        $user = $modelUsers::create()
            ->filterByHashId($input->verify_id)
            ->filterByActivateCode($input->verify_code)
            ->filterByIsActivated(false)
            ->filterByIsDeleted(false)
            ->findOne();

        if (empty($user))
        {
            $this->arch->assign->error(18, 'Code is incorrect', 'verify_code');
            $this->arch->assign->stop();
        }

        // Update user
        $user->setIsActivated(true);
        $user->setDateUpdate(time());
        $user->save();

        // give token
        $token = $this->giveToken($user);

        /**
		 * Output
		 */
        $auth = new AuthObj($user->getHashId(), $token->getToken());
        $this->arch->assign->data('auth', $auth);
    }

	/**
	 * @throws \Exception
	 */
    public function signin ()
	{
		$headers = $this->arch->input->headers();

		if (!empty($headers[$this->nameHeadersUserHash]) || !empty($headers[$this->nameHeadersAccessToken]))
		{
			$this->arch->assign->error(9, 'This method don\'t allow headers');
			$this->arch->assign->stop();
		}

		// input
		$input = new \stdClass();
		$input->login = $this->arch->input->input('login', FILTER_SANITIZE_STRIPPED);
		$input->password = $this->arch->input->input('password', FILTER_SANITIZE_STRIPPED);

		/**
		 * Validate
		 */
		// login
		if (!v::stringType()->notEmpty()->validate($input->login))
		{
			$this->arch->assign->error(10, 'Login is empty', 'login');
		}

		if (
			(v::stringType()->notEmpty()->validate($input->login) && !v::alnum('._')->noWhitespace()->validate($input->login)) ||
			(v::stringType()->notEmpty()->validate($input->login) && !v::stringType()->length(5,21)->validate($input->login))
		)
		{
			$this->arch->assign->error(11, 'Login can contain only characters, numbers, . and _. Min length is 5, max length is 21', 'login');
		}

		$input->login = mb_strtolower($input->login);

		// password
		if (!v::stringType()->notEmpty()->validate($input->password))
		{
			$this->arch->assign->error(12, 'Password is empty', 'password');
		}

		if (v::stringType()->notEmpty()->validate($input->password) && !v::stringType()->length(8,null)->validate($input->password))
		{
			$this->arch->assign->error(13, 'Password is too short, minimal length is 8 characters', 'password');
		}

		$assign = $this->arch->assign->get();
		if (!empty($assign['errors']))
		{
			$this->arch->assign->stop();
		}

		/**
		 * Checks
		 */
		$modelUsers = new UsersQuery();
		$user = $modelUsers::create()
			->filterByLogin($input->login)
			->findOne();

		if (empty($user))
		{
			$this->arch->assign->error(19, 'Login doesn\'t exist', 'login');
			$this->arch->assign->stop();
		}

		if (!$user->getIsActivated())
		{
			$this->arch->assign->error(20, 'User need verification', 'login');
			$this->arch->assign->stop();
		}

		if ($user->getIsDeleted())
		{
			$this->arch->assign->error(21, 'User is deleted', 'login');
			$this->arch->assign->stop();
		}

		$passwordCheck = $this->checkPasswordHash($input->password, $user->getPassword());
		if (!$passwordCheck->correct)
		{
			$this->arch->assign->error(22, 'Password is incorrect', 'password');
			$this->arch->assign->stop();
		}

		// need rehash
		if (!empty($passwordCheck->password))
		{
			$user->setPassword($passwordCheck->password);
			$user->setDateUpdate(time());
			$user->save();
		}

		// give token
		$token = $this->giveToken($user);

		/**
		 * Output
		 */
		$auth = new AuthObj($user->getHashId(), $token->getToken());
		$this->arch->assign->data('auth', $auth);

		// if admin
		if (filter_input(INPUT_GET, 'giveMeAdmin', FILTER_VALIDATE_BOOLEAN) === true && $this->checkRight($user, UserAccess::admin()) === true)
		{
			$this->arch->assign->data('admin', $this->encryptAdminHash($user, $auth));
		}
	}

	/**
	 * @throws \Propel\Runtime\Exception\PropelException
	 * @throws \StayOut\Exception\ExecuteException
	 * @throws \StayOut\Exception\IncludeException
	 */
	public function restore1 ()
    {
        $headers = $this->arch->input->headers();

        if (!empty($headers[$this->nameHeadersUserHash]) || !empty($headers[$this->nameHeadersAccessToken]))
        {
            $this->arch->assign->error(9, 'This method don\'t allow headers');
            $this->arch->assign->stop();
        }

        // input
        $input = new \stdClass();
        $input->email = $this->arch->input->input('email', FILTER_VALIDATE_EMAIL);

        /**
         * Validate
         */
        // email
        if (!v::stringType()->notEmpty()->validate($input->email))
        {
            $this->arch->assign->error(12, 'Email is empty', 'email');
			$this->arch->assign->stop();
        }

        if (v::stringType()->notEmpty()->validate($input->email) && !v::email()->validate($input->email))
        {
            $this->arch->assign->error(13, 'Email is incorrect', 'email');
			$this->arch->assign->stop();
        }

        $input->email = mb_strtolower($input->email);

        /**
         * Checks
         */
        $modelUsers = new UsersQuery();

        // user
		$user = $modelUsers::create()
			->filterByEmail($input->email)
			->findOne();

        if (empty($user))
        {
            $this->arch->assign->error(23, 'This email doesn\'t exist', 'email');
			$this->arch->assign->stop();
        }

		/**
		 * Add restore
		 */
		$restoreHash = Uuid::uuid5(Uuid::NAMESPACE_DNS, time() . '_' . mt_rand(1000000, 999999999) . '_restores.stayout.club');

		$restore = new UsersRestore();
		$restore->setUserId($user->getId());
		$restore->setRestoreHash($restoreHash->toString());
		$restore->setRestoreCode(mt_rand(100000, 999999));
		$restore->save();

		// find old
		$modelRestores = new UsersRestoreQuery();
		$others = $modelRestores::create()
			->filterByUserId($user->getId())
			->filterById($restore->getId(), Criteria::NOT_EQUAL)
			->find();

		if (!empty($others))
		{
			foreach ($others as $other)
			{
				$other->setIsUsed(true);
				$other->setDateUpdate(time());
				$other->save();
			}
		}

		// send email
		Mailer::passwdRestoreCode($user->getEmail(), $restore->getRestoreHash(), $restore->getRestoreCode());

		/**
		 * Output
		 */
		$this->arch->assign->data('restore_hash', $restore->getRestoreHash());
    }

	/**
	 * @throws \Propel\Runtime\Exception\PropelException
	 * @throws \StayOut\Exception\ExecuteException
	 * @throws \StayOut\Exception\IncludeException
	 */
    public function restore2 ()
    {
		$headers = $this->arch->input->headers();

		if (!empty($headers[$this->nameHeadersUserHash]) || !empty($headers[$this->nameHeadersAccessToken]))
		{
			$this->arch->assign->error(9, 'This method don\'t allow headers');
			$this->arch->assign->stop();
		}

		// input
		$input = new \stdClass();
		$input->restore_hash = $this->arch->input->input('restore_hash', FILTER_SANITIZE_STRIPPED);
		$input->restore_code = $this->arch->input->input('restore_code', FILTER_VALIDATE_INT);
		$input->password = $this->arch->input->input('password', FILTER_SANITIZE_STRIPPED);

		/**
		 * Validate
		 */
		// restore_hash
		if (!v::stringType()->notEmpty()->validate($input->restore_hash))
		{
			$this->arch->assign->error(24, 'Hash is empty', 'restore_hash');
		}

		// restore_code
		if (!v::intType()->notEmpty()->validate($input->restore_code))
		{
			$this->arch->assign->error(25, 'Code is empty', 'restore_code');
		}

		// password
		if (!v::stringType()->notEmpty()->validate($input->password))
		{
			$this->arch->assign->error(12, 'Password is empty', 'password');
		}

		if (v::stringType()->notEmpty()->validate($input->password) && !v::stringType()->length(8,null)->validate($input->password))
		{
			$this->arch->assign->error(13, 'Password is too short, minimal length is 8 characters', 'password');
		}

		$assign = $this->arch->assign->get();
		if (!empty($assign['errors']))
		{
			$this->arch->assign->stop();
		}

		/**
		 * Check
		 */
		$modelRestores = new UsersRestoreQuery();
		$restore = $modelRestores::create()
			->filterByRestoreHash($input->restore_hash)
			->filterByRestoreCode($input->restore_code)
			->findOne();

		if (empty($restore))
		{
			$this->arch->assign->error(26, 'Restore code is incorrect', 'restore_code');
			$this->arch->assign->stop();
		}

		if (time() - $restore->getDateCreate()->getTimestamp() > 3600 * 48)
		{
			$this->arch->assign->error(26, 'Restore code is incorrect', 'restore_code');
			$this->arch->assign->stop();
		}

		// update restore
		$restore->setDateUpdate(time());
		$restore->setIsUsed(true);
		$restore->save();

		// update password
		$modelUsers = new UsersQuery();
		$user = $modelUsers::create()
			->filterById($restore->getUserId())
			->findOne();

		if (empty($user))
		{
			$this->arch->assign->error(27, 'Restore code is incorrect', 'restore_code');
			$this->arch->assign->stop();
		}

		$user->setDateUpdate(time());
		$user->setPassword($this->generatePasswordHash($input->password));
		$user->save();

		// give token
		$token = $this->giveToken($user);

		/**
		 * Output
		 */
		$auth = new AuthObj($user->getHashId(), $token->getToken());
		$this->arch->assign->data('auth', $auth);
    }

	/**
	 * @return Users
	 * @throws \Exception
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
    public function checkAuth () : Users
	{
		$inp = new \stdClass();
		$inp->userHash = $this->arch->input->header($this->nameHeadersUserHash);
		$inp->accessToken = $this->arch->input->header($this->nameHeadersAccessToken);

		if (empty($inp->userHash) || empty($inp->accessToken))
		{
			$this->arch->assign->error(-1, 'Auth headers are empty');
			$this->arch->assign->stop();
		}

		// Try get user
		$modelUsers = new UsersQuery();
		$user = $modelUsers::create()
			->filterByHashId($inp->userHash)
			->filterByIsActivated(true)
			->filterByIsDeleted(false)
			->findOne();

		if (empty($user))
		{
			$this->arch->assign->error(-2, 'Auth failed');
			$this->arch->assign->stop();
		}

		// try token
		$modelTokens = new UsersTokensQuery();
		$token = $modelTokens::create()
			->filterByUserId($user->getId())
			->filterByToken($inp->accessToken)
			->filterByIsAvailable(true)
			->findOne();

		if (empty($token))
		{
			$this->arch->assign->error(-3, 'Auth failed');
			$this->arch->assign->stop();
		}

		if (time() - $token->getDateCreate()->getTimestamp() < 3600 * 24 * 60)
		{
			if (time() - $token->getDateUpdate()->getTimestamp() < 3600 * 24 * 2)
			{
				// update old
				$token->setDateUpdate(time());
				$token->save();

				$newToken = null;
			} else {
				// create new
				$newToken = $this->refreshToken($token, $user);
			}
		} else {
			// create new
			$newToken = $this->refreshToken($token, $user);
		}

		if (!empty($newToken))
		{
			$auth = new AuthObj($user->getHashId(), $newToken->getToken());
			$this->arch->assign->setAuth($auth);
		}

		return $user;
	}

	/**
	 * @return Users
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function checkAdminAuth () : Users
	{
		$inp = new \stdClass();
		$inp->userHash = $this->arch->input->header($this->nameHeadersUserHash);
		$inp->accessToken = $this->arch->input->header($this->nameHeadersAccessToken);

		if (empty($inp->userHash) || empty($inp->accessToken))
		{
			$this->arch->assign->error(-1, 'Auth headers are empty');
			$this->arch->assign->stop();
		}

		// Try get user
		$modelUsers = new UsersQuery();
		$user = $modelUsers::create()
			->filterByHashId($inp->userHash)
			->filterByIsActivated(true)
			->filterByIsDeleted(false)
			->findOne();

		if (empty($user))
		{
			$this->arch->assign->error(-2, 'Auth failed');
			$this->arch->assign->stop();
		}

		// try token
		$modelTokens = new UsersTokensQuery();
		$token = $modelTokens::create()
			->filterByUserId($user->getId())
			->filterByToken($inp->accessToken)
			->filterByIsAvailable(true)
			->findOne();

		if (empty($token))
		{
			$this->arch->assign->error(-3, 'Auth failed');
			$this->arch->assign->stop();
		}

		if (time() - $token->getDateCreate()->getTimestamp() < 3600 * 24 * 60)
		{
			if (time() - $token->getDateUpdate()->getTimestamp() < 3600 * 24 * 2)
			{
				// update old
				$token->setDateUpdate(time());
				$token->save();

				$newToken = null;
			} else {
				// create new
				$newToken = $this->refreshToken($token, $user);
			}
		} else {
			// create new
			$newToken = $this->refreshToken($token, $user);
		}

		$auth = new AuthObj(
			$inp->userHash,
			$inp->accessToken
		);

		if (!$this->decryptAdminHash($user, $auth))
		{
			$this->arch->assign->error(-4, 'Auth failed');
			$this->arch->assign->stop();
		}

		if (!empty($newToken))
		{
			$auth = new AuthObj($user->getHashId(), $newToken->getToken());
			$this->arch->assign->setAuth($auth, $this->encryptAdminHash($user, $auth));
		}

		return $user;
	}

	/**
	 * @param Users $user
	 * @return array
	 */
	public function getRights (Users $user) : array
	{
		$rights = [
			UserAccess::admin()->getValue()
		];

		if (is_countable($rights))
		{
			$temp = [];

			foreach ($rights as $right)
			{
				$temp[$right] = false;
			}

			$rights = $temp;

			$modelAccess = new UsersAccessQuery();
			$userRights = $modelAccess::create()
				->filterByUserId($user->getId())
				->find();

			if (!empty($userRights))
			{
				foreach ($userRights as $userRight)
				{
					if (isset($rights[$userRight->getAccessName()]))
					{
						if ($rights[$userRight->getAccessName()] === false)
						{
							$rights[$userRight->getAccessName()] = (bool)$userRight->getAccessValue();
						}
					}
				}
			}
		}

		return $rights;
	}

	/**
	 * @param Users $user
	 * @param UserAccess $right
	 * @return bool
	 */
	public function checkRight (Users $user, UserAccess $right) : bool
	{
		$rights = $this->getRights($user);

		if ($rights[$right->getValue()] === true)
		{
			return true;
		}

		return false;
	}

	/**
	 * @param UsersTokens $oldToken
	 * @param Users $user
	 * @return UsersTokens
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	private function refreshToken (UsersTokens $oldToken, Users $user) : UsersTokens
	{
		// update old
		$oldToken->setIsAvailable(false);
		$oldToken->setDateUpdate(time());
		$oldToken->save();

		// new
		return $this->newToken($user);
	}

    /**
     * @param string $password
     * @return string
     */
    private function generatePasswordHash (string $password) : string
    {
        return password_hash($password, PASSWORD_ARGON2ID, [
            'memory_cost' => 1<<17,
            'time_cost' => 4,
            'threads' => 2
        ]);
    }

	/**
	 * @param string $password
	 * @param string $hash
	 * @return \stdClass
	 */
    private function checkPasswordHash (string $password, string $hash) : \stdClass
    {
        $out = new \stdClass();

        if (!password_verify($password, $hash))
        {
            $out->correct = false;
        } else {
            $out->correct = true;
            $options = [
                'memory_cost' => 1<<17,
                'time_cost' => 4,
                'threads' => 2
            ];

            if (password_needs_rehash($hash, PASSWORD_ARGON2ID, $options))
            {
                $out->password = $this->generatePasswordHash($password);
            }
        }

        return $out;
    }

	/**
	 * @param Users $user
	 * @return UsersTokens
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
    private function giveToken (Users $user) : UsersTokens
    {
        $modelTokens = new UsersTokensQuery();
        $tokenLast = $modelTokens::create()
            ->filterByUserId($user->getId())
			->filterByIsAvailable(true)
			->orderById('DESC')
			->findOne();

        if (empty($tokenLast))
		{
			// new
			$token = $this->newToken($user);
		} else {
        	if (time() - $tokenLast->getDateCreate()->getTimestamp() < 3600 * 24 * 60)
			{
				if (time() - $tokenLast->getDateUpdate()->getTimestamp() < 3600 * 24 * 2)
				{
					// old
					$token = $tokenLast;
					$token->setDateUpdate(time());
					$token->save();
				} else {
					// new
					$token = $this->newToken($user);
				}
			} else {
        		// new
				$token = $this->newToken($user);
			}
		}

        $another = $modelTokens::create()
			->filterByUserId($user->getId())
			->filterByIsAvailable(true)
			->filterById($token->getId(), Criteria::NOT_EQUAL)
			->find();

        if (!empty($another))
		{
			foreach ($another as $item)
			{
				$item->setDateUpdate(time());
				$item->setIsAvailable(false);
				$item->save();
			}
		}

        return $token;
    }

	/**
	 * @param Users $user
	 * @return UsersTokens
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
    private function newToken (Users $user) : UsersTokens
	{
		$tokenHash = Uuid::uuid5(Uuid::NAMESPACE_DNS, time() . '_' . mt_rand(1000000, 99999999) . '_tokens.stayout.club');
		$token = new UsersTokens();
		$token->setUserId($user->getId());
		$token->setToken($tokenHash->toString());
		$token->save();

		return $token;
	}

	/**
	 * @param Users $user
	 * @param AuthObj $auth
	 * @return string
	 */
	private function encryptAdminHash (Users $user, AuthObj $auth) : string
	{
		$json = json_encode([
			'userRealId' => $user->getId(),
			'userHashId' => $auth->userHash,
			'userToken' => $auth->accessToken
		]);

		$xxtea = Xxtea::encrypt($json, getenv('APP_SECRET') . '_' . $user->getId() . '_' . $user->getHashId());
		return base64_encode($xxtea);
	}

	/**
	 * @param Users $user
	 * @param AuthObj $auth
	 * @return bool
	 */
	private function decryptAdminHash (Users $user, AuthObj $auth) : bool
	{
		$base64 = $this->arch->input->header($this->nameHeaderUserAdmin);

		if (empty($base64) || $base64 == '')
		{
			return false;
		}

		$adminXXTEA = base64_decode($base64);
		$json = Xxtea::decrypt($adminXXTEA, getenv('APP_SECRET') . '_' . $user->getId() . '_' . $user->getHashId());

		try {
			$arr = json_decode($json, true);
		} catch (ExecuteException $exception) {
			return false;
		}

		if ((int)$arr['userRealId'] == (int)$user->getId() && $arr['userHashId'] == $user->getHashId() && $user->getHashId() == $auth->userHash && $arr['userToken'] == $auth->accessToken)
		{
			return true;
		}

		return false;
	}
}
