<?php

namespace StayOut\Object;

use StayOut\Assign;
use StayOut\Input;
use StayOut\Module\Auth;
use StayOut\Object\Enum\Langs;

class Arch
{
    /**
     * @var Assign $assign
     */
    public $assign;

    /**
     * @var Input $input
     */
    public $input;

	/**
	 * @var Auth $auth
	 */
    public $auth;

	/**
	 * @var Langs $lang
	 */
    public $lang;

    /**
     * @param Assign $assign
     */
    public function __setAssign (Assign $assign) : void
    {
        $this->assign = $assign;
    }

    /**
     * @param Input $input
     */
    public function __setInput (Input $input) : void
    {
        $this->input = $input;
    }

	/**
	 * Should be launched last
	 * @param Auth $auth
	 */
    public function __setAuth (Auth $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * @param Langs $lang
	 */
	public function __setLang (Langs $lang)
	{
		$this->lang = $lang;
	}
}