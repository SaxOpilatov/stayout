<?php

namespace StayOut;

use Dotenv\Dotenv;
use StayOut\Db\Connection;
use StayOut\DB\SystemErrors;
use StayOut\DB\SystemRequests;
use StayOut\Exception\ExecuteException;
use StayOut\Exception\IncludeException;
use StayOut\Module\Auth;
use StayOut\Object\Arch;
use StayOut\Object\Enum\Langs;
use StayOut\Object\Route;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/includes.php';
require_once __DIR__ . '/objects/arch.php';
require_once __DIR__ . '/objects/abstract/controller.php';
require_once __DIR__ . '/exceptions/exception.php';

class Bootstrap
{
    /**
     * @var Arch $arch;
     */
    private $arch;

    public function __construct()
	{

	}

	private function CORS ()
	{
		$allowDomains = [
			getenv('APP_URL_ADMIN')
		];

		$allowMethods = [
			'POST',
			'PUT'
		];

		$allowHeaders = [
			'Content-Type',
			'X-User-Hash',
			'X-Access-Token',
			'X-Admin-Hash',
			'X-User-Lang'
		];

		if (in_array($_SERVER['HTTP_ORIGIN'], $allowDomains))
		{
			header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
		}

		if (in_array($_SERVER['REQUEST_METHOD'], $allowMethods))
		{
			header('Access-Control-Allow-Methods: ' . $_SERVER['REQUEST_METHOD']);
		}

		header('Access-Control-Allow-Headers: ' . join(", ", $allowHeaders));
		header('X-Powered-By: Your-mommy');
	}

	/**
     * Set Arch
     */
    private function setArch () : void
    {
        $this->arch = new Arch();
        $this->arch->__setAssign(new Assign());
        $this->arch->__setInput(new Input());

        $headerLang = $this->arch->input->header('X-User-Lang');
        if (empty($headerLang))
		{
			$lang = Langs::ru();
		} else {
			if (!in_array($headerLang, Langs::getValues()))
			{
				$lang = Langs::ru();
			} else {
				$lang = Langs::make($headerLang);
			}
		}
        $this->arch->__setLang($lang);

		$auth = new Auth($this->arch);
        $this->arch->__setAuth($auth);
    }

    /**
     * @param Route $route
     * @return systemRequests
     * @throws \Propel\Runtime\Exception\PropelException
     */
    private function addRequestRow (Route $route) : systemRequests
    {
        $request = new SystemRequests();
        $request->setUrl($route->url);
        $request->setMethod($route->method);
        $request->setHasErrors(false);
        $request->setHeaders(json_encode($this->arch->input->headers()));
        $request->setGets(json_encode($this->arch->input->gets()));
        $request->setPosts(json_encode($this->arch->input->posts()));
        $request->setInputs(json_encode($this->arch->input->inputs()));
        $request->setIp($this->arch->input->ip());
        $request->save();

        return $request;
    }


    private function addErrorRow (\Exception $exception, systemRequests $systemRequest) : void
    {
        // save error
        $error = new SystemErrors();
        $error->setRequestId($systemRequest->getId());
        $error->setErrorClass(get_class($exception));
        try {
        	$exc = serialize($exception);
		} catch (\Exception $e) {
			$exc = json_encode($exception);
		}
        $error->setErrorDump(base64_encode($exc));
        $error->save();

        // update request
        $systemRequest->setHasErrors(true);
        $systemRequest->save();
    }

	/**
	 * @throws ExecuteException
	 * @throws \Throwable
	 */
	public function run ()
	{
		// load env
		$env = Dotenv::create(__DIR__ . '/../');
		$env->load();

		if (getenv('APP_DEBUG') === true)
		{
			if (function_exists('xdebug_enable'))
			{
				xdebug_enable();
			}
		} else {
			if (function_exists('xdebug_disable'))
			{
				xdebug_disable();
			}
		}

		// loader
		Includes::Main();

		// enums
        Includes::Enums();

        // modules
        Includes::Modules();

		// load orm
		$connection = new Connection();
		$connection->get();

		// laod router
        $router = new Router();
        $route = $router->get();

        // set arch
        $this->setArch();

        // cors
		$this->CORS();

        // write request
        try
        {
            $request = $this->addRequestRow($route);
        } catch (\Exception $e) {
            throw new ExecuteException($e->getMessage(), $e->getCode());
        }

        try {
            if ($route->url != '/graphql')
            {
                if (Includes::Controller($route))
                {
                    // routing
                    if (!class_exists($route->action))
                    {
                        throw new IncludeException('Can\'t find executable class', 6);
                    } else {
                        $controller = new $route->action($this->arch);
                        if (!method_exists($controller, 'index'))
                        {
                            throw new IncludeException('Can\'t find executable method', 7);
                        } else {
                            try {
                                $controller->index();
                            } catch (\Exception $e) {
                                throw new ExecuteException($e->getMessage(), $e->getCode());
                            }
                        }
                    }
                } else {
                    throw new IncludeException('Can\'t find controller', 5);
                }
            } else {
                // graphql
                try {
					$graphQL = new GraphQL();
					$graphQL->run($this->arch);
				} catch (\Exception $e) {
					throw new ExecuteException($e->getMessage(), $e->getCode());
				}
            }

            $debug = [];
            $errorCode = 0;
            $errorMessage = '';
        } catch (\Exception $e) {
            $this->addErrorRow($e, $request);
            $debug = $e->getTrace();
            $errorCode = $e->getCode();
            $errorMessage = $e->getMessage();
        }

        $data = $this->arch->assign->get();
        if (getenv('APP_DEBUG') === true)
        {
            $data['errorTrace'] = $debug;
            $data['errorDesc'] = [
                'code' => $errorCode,
                'message' => $errorMessage
            ];
        }

        header('Content-Type: application/json');
        echo json_encode([
        	'response' => $data
		]);
	}
}
