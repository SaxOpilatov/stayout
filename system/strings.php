<?php

namespace StayOut;

class Strings
{
    public static function mbReplace($search, $replace, $subject, $encoding = 'auto', &$count=0)
    {
        if(!is_array($subject))
        {
            $searches = is_array($search) ? array_values($search) : [$search];
            $replacements = is_array($replace) ? array_values($replace) : [$replace];
            $replacements = array_pad($replacements, count($searches), '');
            foreach($searches as $key => $search)
            {
                $replace = $replacements[$key];
                $search_len = mb_strlen($search, $encoding);

                $sb = [];
                while(($offset = mb_strpos($subject, $search, 0, $encoding)) !== false)
                {
                    $sb[] = mb_substr($subject, 0, $offset, $encoding);
                    $subject = mb_substr($subject, $offset + $search_len, null, $encoding);
                    ++$count;
                }
                $sb[] = $subject;
                $subject = implode($replace, $sb);
            }
        } else {
            foreach($subject as $key => $value)
            {
                $subject[$key] = self::mbReplace($search, $replace, $value, $encoding, $count);
            }
        }

        return $subject;
    }
}