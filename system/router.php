<?php

namespace StayOut;

use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\RouteCollector;
use StayOut\Controller\Defaults\Error404Action;
use StayOut\Object\Enum\HttpMethods;
use StayOut\Object\Route;

class Router
{
	/**
	 * @var string $path
	 */
	private $path = __DIR__ . '/routes/';

	public function get () : Route
	{
		require_once __DIR__ . '/objects/route.php';

        if (!empty(filter_input(INPUT_GET, '_url', FILTER_SANITIZE_STRING)))
        {
            $url = filter_input(INPUT_GET, '_url', FILTER_SANITIZE_STRING);
        } else {
            $url = '/';
        }

        if ($url == 'graphql')
        {
            return new Route(
                HttpMethods::any(),
                $url,
                GraphQL::class
            );
        } else {
            $routes = [];

            $files = scandir($this->path);
            if (is_countable($files))
            {
                for ($i = 2; $i < count($files); $i++)
                {
                    $routes = array_merge($routes, require_once($this->path . $files[$i]));
                }
            }

            $router = new RouteCollector();

            if (is_countable($routes))
            {
                foreach ($routes as $route)
                {
                    $router->{$route->method->getValue()}($route->url, function () use ($route)
                    {
                        return $route;
                    });
                }
            }

            $dispatcher = new Dispatcher($router->getData());

            try {
                return $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $url);
            } catch (\Exception $exception) {
                return new Route(
                    HttpMethods::any(),
                    $url,
                    Error404Action::class
                );
            }
        }
	}
}