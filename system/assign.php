<?php

namespace StayOut;

use StayOut\Exception\AssignException;
use StayOut\Object\Auth;

class Assign
{
    /**
     * @var array $output
     */
    private $output;

    /**
     * Assign constructor.
     */
    public function __construct()
    {
        $this->output = [
            'data' => [],
            'errors' => [],
			'auth_update' => [
				'need' => false,
				'userHash' => '',
				'accessToken' => ''
			]
        ];
    }

    /**
     * @param string $key
     * @param $value
     */
    public function data (string $key, $value) : void
    {
        $this->output['data'][$key] = $value;
    }

	/**
	 * @param array $data
	 */
    public function rewriteData (array $data) : void
	{
		$this->output['data'] = $data;
	}

    /**
     * @param int $code
     * @param string $error
     * @param string|null $element
     */
    public function error (int $code, string $error, ?string $element = null) : void
    {
        $this->output['errors'][] = [
            'code' => $code,
            'message' => $error,
            'element' => empty($element) ? '' : $element
        ];
    }

    /**
     * @throws \Exception
     */
    public function stop () : void
    {
        $this->output['data']=[];
        throw new AssignException('Stop app', 2);
    }

	/**
	 * @param array $errors
	 * @throws AssignException
	 */
    public function stopGraph(array $errors) : void
	{
		foreach ($errors as $i => $error)
		{
			$errors[$i]['code'] = 30;
		}

		$this->output['errors_graph'] = $errors;
		$this->output['data']=[];

		throw new AssignException('Stop app', 2);
	}

	/**
	 * @param Auth $auth
	 * @param string $admin
	 */
	public function setAuth (Auth $auth, string $admin = '')
	{
		$this->output['auth_update']['need'] = true;
		$this->output['auth_update']['userHash'] = $auth->userHash;
		$this->output['auth_update']['accessToken'] = $auth->accessToken;

		if (!empty($admin))
		{
			$this->output['auth_update']['adminHash'] = $admin;
		}
	}

    /**
     * @return array
     */
    public function get () : array
    {
        return $this->output;
    }
}
