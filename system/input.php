<?php

namespace StayOut;

class Input
{
	/**
	 * @param string $key
	 * @return string
	 */
    public function header (string $key) : string
    {
        $headers = getallheaders();
        if(!empty($headers[$key]))
        {
            $ret = (string)$headers[$key];
        } else {
            $ret = "";
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function headers () : array
    {
        return getallheaders();
    }

    /**
     * @param string $key
     * @param int|null $filter
     * @return mixed|null
     */
    public function input (string $key, ?int $filter = FILTER_SANITIZE_STRIPPED)
    {
        $arr = $this->inputs();

        if (empty($arr[$key]))
        {
            return null;
        } else {
            return filter_var($arr[$key], $filter);
        }
    }

    /**
     * @return array
     */
    public function inputs () : array
    {
        $input = file_get_contents("php://input");
        if (empty($input))
        {
            return [];
        } else {
            try {
                $arr = json_decode($input, true);
            } catch (\Exception $e) {
                $arr = [];
            }

            if (is_null($arr))
            {
                $arr = [];
            }

            return $arr;
        }
    }

    /**
     * @return array
     */
    public function posts () : array
    {
        if (empty($_POST))
        {
            return [];
        } else {
            return filter_input_array(INPUT_POST);
        }
    }

    /**
     * @return array
     */
    public function gets () : array
    {
        if (empty($_GET))
        {
            return [];
        } else {
            return filter_input_array(INPUT_GET);
        }
    }

    /**
     * @return string
     */
    public function ip () : string
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
}

/**
 * if nginx
 */
if (!function_exists('getallheaders'))
{
    /**
     * @return array
     */
    function getallheaders()
    {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        return $headers;
    }
}
