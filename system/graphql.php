<?php

namespace StayOut;

use GraphQL\Error\Debug;
use GraphQL\Error\FormattedError;
use GraphQL\GraphQL as GraphQLLib;
use GraphQL\Type\Schema;
use StayOut\Exception\ExecuteException;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\Arch;
use StayOut\Object\Types;

class GraphQL
{
	/**
	 * @param Arch $arch
	 * @throws \Throwable
	 */
    public function run (Arch $arch)
    {
    	require_once __DIR__ . '/objects/types.php';
    	require_once __DIR__ . '/objects/abstract/controller-graphql.php';

		if (getenv('APP_DEBUG') === true)
		{
			set_error_handler(function($severity, $message, $file, $line) use (&$phpErrors) {
				throw new \ErrorException($message, 0, $severity, $file, $line);
			});

			$debug = Debug::INCLUDE_DEBUG_MESSAGE | Debug::INCLUDE_TRACE;
		} else {
			$debug = false;
		}

		try {
			if (isset($_SERVER['CONTENT_TYPE']) && strpos($_SERVER['CONTENT_TYPE'], 'application/json') !== false)
			{
				$raw = file_get_contents('php://input') ?: '';
				$data = json_decode($raw, true) ?: [];
			} else {
				$data = $_REQUEST;
			}

			$data += ['query' => null, 'variables' => null];

			if ($data['query'] === null)
			{
				$data['query'] = '{hello}';
			}

			$schema = new Schema([
				'query' => Types::query(),
				'mutation' => Types::mutation()
			]);

			$result = GraphQLLib::executeQuery(
				$schema,
				$data['query'],
				null,
				$arch,
				(array)$data['variables']
			);
			$output = $result->toArray($debug);
		} catch (\Exception $error) {
			$output['errors'] = [
				FormattedError::createFromException($error, $debug)
			];
		}

		try {
			if (!empty($output['errors']))
			{
				$arch->assign->stopGraph($output['errors']);
			} else {
				$arch->assign->rewriteData($output['data']);
			}
		} catch (\Exception $e) {
			throw new ExecuteException($e->getMessage(), $e->getCode());
		}
    }

	/**
	 * @param string $controller
	 * @param $rootValue
	 * @param $args
	 * @param Arch $arch
	 * @return null|array|object
	 * @throws ExecuteException
	 */
    public static function execute (string $controller, $rootValue, $args, Arch $arch)
	{
		list($_, $_, $controllerPart, $controllerName) = explode("\\", $controller);
		$controllerFile = __DIR__ . '/../app/Controllers/' . mb_strtolower($controllerPart) . '/' . mb_convert_case(str_replace("Action", "", $controllerName), MB_CASE_TITLE) . '.php';

		if (!file_exists($controllerFile))
		{
			throw new ExecuteException('Bad controller', 31);
		}

		require_once $controllerFile;

		try {
			$ctrl = new $controller($arch, $args, $rootValue);

			if (!is_subclass_of($ctrl, ControllerGraphQL::class))
			{
				throw new ExecuteException('Bad controller' , 33);
			}

			if (!method_exists($ctrl, 'index'))
			{
				throw new ExecuteException('Bad controller: method', 34);
			}

			return $ctrl->index();
		} catch (ExecuteException $e) {
			throw new ExecuteException('Bad controller' , 32);
		}
	}
}
