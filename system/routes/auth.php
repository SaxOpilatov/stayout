<?php

use StayOut\Object\Route;
use StayOut\Object\Enum\HttpMethods;

return [
    new Route(
        HttpMethods::post(),
        '/auth/signup',
        \StayOut\Controller\Auth\SignupAction::class
    ),

    new Route(
        HttpMethods::post(),
        '/auth/verify',
        \StayOut\Controller\Auth\VerifyAction::class
    ),

	new Route(
		HttpMethods::post(),
		'/auth/signin',
		\StayOut\Controller\Auth\SigninAction::class
	),

    new Route(
        HttpMethods::put(),
        '/auth/restore',
        \StayOut\Controller\Auth\Restore1Action::class
    ),

    new Route(
        HttpMethods::post(),
        '/auth/restore',
        \StayOut\Controller\Auth\Restore2Action::class
    )
];