<?php

namespace StayOut\Controller\Defaults;

use StayOut\Object\Abstracts\Controller;

class Error404Action extends Controller
{
    public function index()
    {
        $this->arch->assign->error(8, 'Url doesn\'t exist');
        $this->arch->assign->stop();
    }
}