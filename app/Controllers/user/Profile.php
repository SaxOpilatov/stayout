<?php

namespace StayOut\Controller\User;

use Ramsey\Uuid\Uuid;
use StayOut\DB\DataCitiesQuery;
use StayOut\DB\DataCountries;
use StayOut\DB\DataCountriesQuery;
use StayOut\DB\UsersFields;
use StayOut\DB\UsersFieldsQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\Enum\UserFields;
use StayOut\Object\GraphqlTypes\Data\AirImage;
use StayOut\Object\GraphqlTypes\Data\DataCity;
use StayOut\Object\GraphqlTypes\Data\DataCountry;
use StayOut\Object\GraphqlTypes\Data\User;

class ProfileAction extends ControllerGraphQL
{
	/**
	 * @return User
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function index() : User
	{
		$myself = $this->arch->auth->checkAuth();
		$getNameLang = 'getName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);

		$modelFields = new UsersFieldsQuery();
		$vals = $modelFields::create()
			->filterByUserId($myself->getId())
			->find();

		$dataVals = [];
		if (!empty($vals))
		{
			/**
			 * @var UsersFields $val
			 */
			foreach ($vals as $val)
			{
				$dataVals[$val->getFieldName()] = $val->getFieldValue();
			}
		}

		// Country
		if (empty($dataVals[UserFields::country()->getValue()]))
		{
			$country = new DataCountry([]);
		} else {
			$modelCountries = new DataCountriesQuery();
			$selectCountry = $modelCountries::create()
				->filterById($dataVals[UserFields::country()->getValue()])
				->findOne();

			if (empty($selectCountry))
			{
				$country = new DataCountry([]);
			} else {
				$country = new DataCountry([
					'id' => $selectCountry->getHashId(),
					'code' => $selectCountry->getCode(),
					'name' => $selectCountry->{$getNameLang}()
				]);
			}
		}

		// City
		if (empty($dataVals[UserFields::city()->getValue()]))
		{
			$city = new DataCity([]);
		} else {
			$modelCities = new DataCitiesQuery();
			$selectCity = $modelCities::create()
				->filterById($dataVals[UserFields::city()->getValue()])
				->findOne();

			if (empty($selectCity))
			{
				$city = new DataCity([]);
			} else {
				$city = new DataCity([
					'id' => $selectCity->getHashId(),
					'name' => $selectCity->{$getNameLang}(),
					'country' => $country
				]);
			}
		}

		// Full name
		if (empty($dataVals[UserFields::full_name()->getValue()]))
		{
			$full_name = "";
		} else {
			$full_name = $dataVals[UserFields::full_name()->getValue()];
		}

		// Image
		if (empty($dataVals[UserFields::image()->getValue()]))
		{
			$image = new AirImage([]);
		} else {
			$image = new AirImage([
				'path' => $dataVals[UserFields::image()->getValue()]
			]);
		}

		// Sex
		if (empty($dataVals[UserFields::sex()->getValue()]))
		{
			$sex = 0;
		} else {
			$sex = (int)$dataVals[UserFields::sex()->getValue()];
		}

		// Birth date
		if (empty($dataVals[UserFields::birth_date()->getValue()]))
		{
			$birth_date = "";
		} else {
			try {
				$date = new \DateTime($dataVals[UserFields::birth_date()->getValue()]);
				$birth_date = $date->format('Y-m-d');
			} catch (\Exception $e) {
				$birth_date = "";
			}
		}

		if (empty($dataVals[UserFields::about()->getValue()]))
		{
			$about = "";
		} else {
			$about = $dataVals[UserFields::about()->getValue()];
		}

		return new User([
			'id' => $myself->getHashId(),
			'login' => $myself->getLogin(),
			'email' => $myself->getEmail(),
			'country' => $country,
			'city' => $city,
			'full_name' => $full_name,
			'image' => $image,
			'sex' => $sex,
			'birth_date' => $birth_date,
			'about' => $about
		]);
	}
}