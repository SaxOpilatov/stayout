<?php

namespace StayOut\Controller\User;

use StayOut\Object\Abstracts\ControllerGraphQL;

class AdminLinksAction extends ControllerGraphQL
{
	/**
	 * @return string[]
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function index() : array
	{
		$myself = $this->arch->auth->checkAdminAuth();
		$rights = $this->arch->auth->getRights($myself);

		$access = [];
		if (!empty($rights))
		{
			foreach ($rights as $right => $availability)
			{
				if ($availability)
				{
					$access[] = $right;
				}
			}
		}

		return $access;
	}
}
