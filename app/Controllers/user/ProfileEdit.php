<?php

namespace StayOut\Controller\User;

use Propel\Runtime\ActiveQuery\Criteria;
use StayOut\DB\DataCitiesQuery;
use StayOut\DB\DataCountriesQuery;
use StayOut\DB\UsersFields;
use StayOut\DB\UsersFieldsQuery;
use StayOut\DB\UsersQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\Enum\UserFields;
use StayOut\Object\GraphqlTypes\Data\User;
use Respect\Validation\Validator as v;

class ProfileEditAction extends ControllerGraphQL
{
	/**
	 * @return User
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function index() : User
	{
		$myself = $this->arch->auth->checkAuth();

		$input = new \stdClass();
		empty($this->args['login']) ? $input->login = null : $input->login = filter_var($this->args['login'], FILTER_SANITIZE_STRIPPED);
		!isset($this->args['country']) ? $input->country = null : $input->country = filter_var($this->args['country'], FILTER_SANITIZE_STRIPPED);
		!isset($this->args['city']) ? $input->city = null : $input->city = filter_var($this->args['city'], FILTER_SANITIZE_STRIPPED);
		!isset($this->args['full_name']) ? $input->full_name = null : $input->full_name = filter_var($this->args['full_name'], FILTER_SANITIZE_STRIPPED);
		!isset($this->args['image']) ? $input->image = null : $input->image = filter_var($this->args['image'], FILTER_SANITIZE_STRIPPED);
		!isset($this->args['sex']) ? $input->sex = null : $input->sex = filter_var($this->args['sex'], FILTER_SANITIZE_NUMBER_INT);
		!isset($this->args['birth_date']) ? $input->birth_date = null : $input->birth_date = filter_var($this->args['birth_date'], FILTER_SANITIZE_STRIPPED);
		!isset($this->args['about']) ? $input->about = null : $input->about = filter_var($this->args['about'], FILTER_SANITIZE_STRIPPED);

		$modelUsers = new UsersQuery();
		$modelFields = new UsersFieldsQuery();
		$somethingChanged = false;

		// login
		if ($input->login !== null)
		{
			if (
				(v::stringType()->notEmpty()->validate($input->login) && !v::alnum('._')->noWhitespace()->validate($input->login)) ||
				(v::stringType()->notEmpty()->validate($input->login) && !v::stringType()->length(5,21)->validate($input->login))
			)
			{
				$this->arch->assign->error(11, 'Login can contain only characters, numbers, . and _. Min length is 5, max length is 21', 'login');
			} else {
				$countLogins = $modelUsers::create()
					->filterByLogin($input->login)
					->filterById($myself->getId(), Criteria::ALT_NOT_EQUAL)
					->count();

				if ($countLogins > 0)
				{
					$this->arch->assign->error(15, 'This login is already exist', 'login');
				} else {
					$somethingChanged = true;
					$myself->setLogin(mb_strtolower($input->login));
				}
			}
		}

		// country
		if ($input->country !== null)
		{
			$countryField = $modelFields::create()
				->filterByUserId($myself->getId())
				->filterByFieldName(UserFields::country()->getValue())
				->findOne();

			if ($input->country == "")
			{
				if (!empty($countryField))
				{
					$somethingChanged = true;
					$countryField->delete();
				}
			} else {
				$modelCountries = new DataCountriesQuery();
				$country = $modelCountries::create()
					->filterByHashId($input->country)
					->findOne();

				if (empty($country))
				{
					$this->arch->assign->error(35, 'Country doesn\'t exist');
					$this->arch->assign->stop();
				}

				if (empty($countryField))
				{
					$countryField = new UsersFields();
					$countryField->setUserId($myself->getId());
					$countryField->setFieldName(UserFields::country()->getValue());
				}

				$countryField->setFieldValue($country->getId());
				$countryField->save();
				$somethingChanged = true;
			}
		}

		// city
		if ($input->city !== null)
		{
			$cityField = $modelFields::create()
				->filterByUserId($myself->getId())
				->filterByFieldName(UserFields::city()->getValue())
				->findOne();

			if ($input->city == "")
			{
				if (!empty($cityField))
				{
					$somethingChanged = true;
					$cityField->delete();
				}
			} else {
				$modelCities = new DataCitiesQuery();
				$city = $modelCities::create()
					->filterByHashId($input->city)
					->findOne();

				if (empty($city))
				{
					$this->arch->assign->error(36, 'City doesn\'t exist');
					$this->arch->assign->stop();
				}

				$countryField = $modelFields::create()
					->filterByUserId($myself->getId())
					->filterByFieldName(UserFields::country()->getValue())
					->findOne();

				if ((int)$countryField->getFieldValue() != (int)$city->getCountryId())
				{
					$this->arch->assign->error(37, 'City doesn\'t exist');
					$this->arch->assign->stop();
				}

				if (empty($cityField))
				{
					$cityField = new UsersFields();
					$cityField->setUserId($myself->getId());
					$cityField->setFieldName(UserFields::city()->getValue());
				}

				$cityField->setFieldValue($city->getId());
				$cityField->save();
				$somethingChanged = true;
			}
		}

		// full_name
		if ($input->full_name !== null)
		{
			$fullNameField = $modelFields::create()
				->filterByUserId($myself->getId())
				->filterByFieldName(UserFields::full_name()->getValue())
				->findOne();

			if ($input->full_name == "")
			{
				if (!empty($fullNameField))
				{
					$somethingChanged = true;
					$fullNameField->delete();
				}
 			} else {
				if (empty($fullNameField))
				{
					$fullNameField = new UsersFields();
					$fullNameField->setUserId($myself->getId());
					$fullNameField->setFieldName(UserFields::full_name()->getValue());
				}

				$fullNameField->setFieldValue($input->full_name);
				$fullNameField->save();
				$somethingChanged = true;
			}
		}

		// image
		if ($input->image !== null)
		{
			$imageField = $modelFields::create()
				->filterByUserId($myself->getId())
				->filterByFieldName(UserFields::image()->getValue())
				->findOne();

			if ($input->image == "")
			{
				$somethingChanged = true;
				$imageField->delete();
			} else {
				/*
				 * ToDo
				 * Check image path
				 */

				if (empty($imageField))
				{
					$imageField = new UsersFields();
					$imageField->setUserId($myself->getId());
					$imageField->setFieldName(UserFields::image()->getValue());
				}

				$imageField->setFieldValue($input->image);
				$imageField->save();
				$somethingChanged = true;
			}
		}

		// sex
		if ($input->sex !== null)
		{
			$sexField = $modelFields::create()
				->filterByUserId($myself->getId())
				->filterByFieldName(UserFields::sex()->getValue())
				->findOne();

			if ($input->sex == 0)
			{
				$somethingChanged = true;
				$sexField->delete();
			} else {
				if (empty($sexField))
				{
					$sexField = new UsersFields();
					$sexField->setUserId($myself->getId());
					$sexField->setFieldName(UserFields::sex()->getValue());
				}

				$sexField->setFieldValue($input->sex);
				$sexField->save();
				$somethingChanged = true;
			}
		}

		// birth_date
		if ($input->birth_date !== null)
		{
			$birthDateField = $modelFields::create()
				->filterByUserId($myself->getId())
				->filterByFieldName(UserFields::birth_date()->getValue())
				->findOne();

			if ($input->birth_date == "")
			{
				$somethingChanged = true;
				$birthDateField->delete();
			} else {
				try {
					$date = new \DateTime($input->birth_date);
				} catch (\Exception $e) {
					$this->arch->assign->error(38, 'Invalid date format');
					$this->arch->assign->stop();
				}

				if (empty($birthDateField))
				{
					$birthDateField = new UsersFields();
					$birthDateField->setUserId($myself->getId());
					$birthDateField->setFieldName(UserFields::birth_date()->getValue());
				}

				$birthDateField->setFieldValue($date->format("Y-m-d"));
				$birthDateField->save();
				$somethingChanged = true;
			}
		}

		// about
		if ($input->about !== null)
		{
			$aboutField = $modelFields::create()
				->filterByUserId($myself->getId())
				->filterByFieldName(UserFields::about()->getValue())
				->findOne();

			if ($input->about == "")
			{
				$somethingChanged = true;
				$aboutField->delete();
			} else {
				if (empty($aboutField))
				{
					$aboutField = new UsersFields();
					$aboutField->setUserId($myself->getId());
					$aboutField->setFieldName(UserFields::about()->getValue());
				}

				$aboutField->setFieldValue($input->about);
				$aboutField->save();
				$somethingChanged = true;
			}
		}

		// save user and update date_update
		if ($somethingChanged)
		{
			$myself->setDateUpdate(time());
			$myself->save();
		}

		require_once __DIR__ . '/Profile.php';
		$userController = new ProfileAction($this->arch, $this->args, $this->rootValue);
		return $userController->index();
	}
}