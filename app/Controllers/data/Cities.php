<?php

namespace StayOut\Controller\Data;

use Propel\Runtime\ActiveQuery\Criteria;
use StayOut\DB\DataCities;
use StayOut\DB\DataCitiesQuery;
use StayOut\DB\DataCountriesQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\GraphqlTypes\Data\DataCity;
use StayOut\Object\GraphqlTypes\Data\DataCountry;

class CitiesAction extends ControllerGraphQL
{
	/**
	 * @return DataCity[]
	 * @throws \Exception
	 */
	public function index() : array
	{
		$dataCities = [];
		$modelCities = new DataCitiesQuery();
		$modelCountries = new DataCountriesQuery();

		$cities = $modelCities::create();

		if (!empty($this->args['country']))
		{
			$country = $modelCountries::create()
				->filterByHashId($this->args['country'])
				->findOne();

			if (empty($country))
			{
				$this->arch->assign->error(37, 'Country doesn\'t exist');
				$this->arch->assign->stop();
			}

			$cities->filterByCountryId($country->getId());
		}

		if (!empty($this->args['name']))
		{
			$filterLang = 'filterByName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);
			$cities->{$filterLang}('%' . $this->args['name'] . '%', Criteria::ILIKE);
		}

		$cities->find();

		if (!empty($cities))
		{
			$countries = $modelCountries::create()->find();
			$dataCountries = [];

			if (!empty($countries))
			{
				$countryName = 'getName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);

				foreach ($countries as $country)
				{
					$dataCountries[$country->getId()] = new DataCountry([
						'id' => $country->getHashId(),
						'code' => $country->getCode(),
						'name' => $country->{$countryName}()
					]);
				}
			}

			/**
			 * @var DataCities $city
			 */
			$nameLang = 'getName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);
			foreach ($cities as $city)
			{
				if (empty($dataCountries[$city->getCountryId()]))
				{
					$country = new DataCountry([]);
				} else {
					$country = $dataCountries[$city->getCountryId()];
				}

				$dataCities[] = new DataCity([
					'id' => $city->getHashId(),
					'name' => $city->{$nameLang}(),
					'country' => $country
				]);
			}
		}

		return $dataCities;
	}
}