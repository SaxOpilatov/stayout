<?php

namespace StayOut\Controller\Data;

use StayOut\DB\DataCitiesQuery;
use StayOut\DB\DataCountriesQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\GraphqlTypes\Data\DataCity;
use StayOut\Object\GraphqlTypes\Data\DataCountry;

class CityAction extends ControllerGraphQL
{
	/**
	 * @return DataCity
	 * @throws \Exception
	 */
	public function index() : DataCity
	{
		$modelCities = new DataCitiesQuery();
		$city = $modelCities::create()
			->filterByHashId($this->args['id'])
			->findOne();

		if (empty($city))
		{
			$this->arch->assign->error(36, 'City doesn\'t exist');
			$this->arch->assign->stop();
		}

		$modelCountries = new DataCountriesQuery();
		$country = $modelCountries::create()
			->filterById($city->getCountryId())
			->findOne();

		if (empty($country))
		{
			$this->arch->assign->error(35, 'Country doesn\'t exist');
			$this->arch->assign->stop();
		}

		$cityAndCountryName = 'getName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);

		return new DataCity([
			'id' => $city->getHashId(),
			'name' => $city->{$cityAndCountryName}(),
			'country' => new DataCountry([
				'id' => $country->getHashId(),
				'code' => $country->getCode(),
				'name' => $country->{$cityAndCountryName}()
			])
		]);
	}
}