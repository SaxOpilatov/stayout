<?php

namespace StayOut\Controller\Data;

use StayOut\DB\DataCountriesQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\GraphqlTypes\Data\DataCountry;

class CountryAction extends ControllerGraphQL
{
	/**
	 * @return DataCountry
	 * @throws \Exception
	 */
	public function index() : DataCountry
	{
		$modelCountries = new DataCountriesQuery();
		$country = $modelCountries::create()
			->filterByHashId($this->args['id'])
			->findOne();

		if (empty($country))
		{
			$this->arch->assign->error(35, 'Country doesn\'t exist');
			$this->arch->assign->stop();
		}

		$countryName = 'getName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);

		return new DataCountry([
			'id' => $country->getHashId(),
			'code' => $country->getCode(),
			'name' => $country->{$countryName}()
		]);
	}
}