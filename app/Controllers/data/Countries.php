<?php

namespace StayOut\Controller\Data;

use Propel\Runtime\ActiveQuery\Criteria;
use StayOut\DB\DataCountries;
use StayOut\DB\DataCountriesQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\GraphqlTypes\Data\DataCountry;

class CountriesAction extends ControllerGraphQL
{
	/**
	 * @return DataCountry[]
	 * @throws \Exception
	 */
	public function index() : array
	{
		$nameLang = 'getName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);
		$dataCountries = [];
		$modelCountries = new DataCountriesQuery();

		if (empty($this->args['name']))
		{
			$countries = $modelCountries::create()->find();
		} else {
			$filterLang = 'filterByName' . mb_convert_case($this->arch->lang->getValue(), MB_CASE_TITLE);

			$countries = $modelCountries::create()
				->{$filterLang}('%' . $this->args['name'] . '%', Criteria::ILIKE)
				->find();
		}

		if (!empty($countries))
		{
			foreach ($countries as $country)
			{
				/**
				 * @var DataCountries $country
				 */
				$dataCountries[] = new DataCountry([
					'id' => $country->getHashId(),
					'code' => $country->getCode(),
					'name' => $country->{$nameLang}()
				]);
			}
		}

		return $dataCountries;
	}
}