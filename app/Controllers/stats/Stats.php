<?php

namespace StayOut\Controller\Stats;

use Propel\Runtime\ActiveQuery\Criteria;
use StayOut\DB\UsersQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\GraphqlTypes\Data\Stats;

class StatsAction extends ControllerGraphQL
{
	/**
	 * @return Stats
	 * @throws \Exception
	 */
	public function index() : Stats
	{
		$this->arch->auth->checkAdminAuth();

		// Users
		$modelUsers = new UsersQuery();

		$usersCount = $modelUsers::create()->count();

		$date = new \DateTime();
		$usersLast7daysCount = $modelUsers::create()
			->filterByDateCreate($date->modify("-1 day")->getTimestamp(), Criteria::LESS_EQUAL)
			->filterByDateCreate($date->modify("-6 days")->getTimestamp(), Criteria::GREATER_EQUAL)
			->count();

		$date = new \DateTime();
		$usersLast1monthCount = $modelUsers::create()
			->filterByDateCreate($date->modify("-1 day")->getTimestamp(), Criteria::LESS_EQUAL)
			->filterByDateCreate($date->modify("-1 month")->getTimestamp(), Criteria::GREATER_EQUAL)
			->count();

		$date = new \DateTime();
		$usersLast3monthsCount = $modelUsers::create()
			->filterByDateCreate($date->modify("-1 day")->getTimestamp(), Criteria::LESS_EQUAL)
			->filterByDateCreate($date->modify("-3 months")->getTimestamp(), Criteria::GREATER_EQUAL)
			->count();

		$date = new \DateTime();
		$usersLast6monthsCount = $modelUsers::create()
			->filterByDateCreate($date->modify("-1 day")->getTimestamp(), Criteria::LESS_EQUAL)
			->filterByDateCreate($date->modify("-6 months")->getTimestamp(), Criteria::GREATER_EQUAL)
			->count();

		return new Stats([
			'users' => $usersCount,
			'usersLast7days' => $usersLast7daysCount,
			'usersLast1month' => $usersLast1monthCount,
			'usersLast3months' => $usersLast3monthsCount,
			'usersLast6months' => $usersLast6monthsCount,
			'places' => 0,
			'placesLast7days' => 0,
			'placesLast1month' => 0,
			'placesLast3months' => 0,
			'placesLast6months' => 0,
			'events' => 0,
			'eventsLast7days' => 0,
			'eventsLast1month' => 0,
			'eventsLast3months' => 0,
			'eventsLast6months' => 0
		]);
	}
}
