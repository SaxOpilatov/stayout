<?php

namespace StayOut\Controller\Auth;

use StayOut\Object\Abstracts\Controller;

class VerifyAction extends Controller
{
	/**
	 * @throws \Exception
	 */
	public function index ()
	{
		$this->arch->auth->verify();
	}
}