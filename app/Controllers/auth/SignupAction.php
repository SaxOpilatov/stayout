<?php

namespace StayOut\Controller\Auth;

use StayOut\Object\Abstracts\Controller;

class SignupAction extends Controller
{
    /**
     * @throws \Exception
     */
    public function index ()
    {
		$this->arch->auth->signup();
    }
}