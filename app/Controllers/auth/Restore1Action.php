<?php

namespace StayOut\Controller\Auth;

use StayOut\Object\Abstracts\Controller;

class Restore1Action extends Controller
{
    /**
     * @throws \Exception
     */
    public function index ()
    {
        $this->arch->auth->restore1();
    }
}