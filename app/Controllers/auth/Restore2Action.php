<?php

namespace StayOut\Controller\Auth;

use StayOut\Object\Abstracts\Controller;

class Restore2Action extends Controller
{
    /**
     * @throws \Exception
     */
    public function index ()
    {
        $this->arch->auth->restore2();
    }
}