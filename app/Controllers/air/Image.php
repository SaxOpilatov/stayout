<?php

namespace StayOut\Controller\Air;

use StayOut\DB\DataCountriesQuery;
use StayOut\Object\Abstracts\ControllerGraphQL;
use StayOut\Object\GraphqlTypes\Data\AirImage;

class ImageAction extends ControllerGraphQL
{
	/**
	 * @return AirImage
	 * @throws \Exception
	 */
	public function index() : AirImage
	{
		return new AirImage([
			'path' => $this->args['path']
		]);
	}
}