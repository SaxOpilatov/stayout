<?php

namespace StayOut\DB\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use StayOut\DB\UsersFields as ChildUsersFields;
use StayOut\DB\UsersFieldsQuery as ChildUsersFieldsQuery;
use StayOut\DB\Map\UsersFieldsTableMap;

/**
 * Base class that represents a query for the 'users_fields' table.
 *
 *
 *
 * @method     ChildUsersFieldsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUsersFieldsQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildUsersFieldsQuery orderByFieldName($order = Criteria::ASC) Order by the field_name column
 * @method     ChildUsersFieldsQuery orderByFieldValue($order = Criteria::ASC) Order by the field_value column
 *
 * @method     ChildUsersFieldsQuery groupById() Group by the id column
 * @method     ChildUsersFieldsQuery groupByUserId() Group by the user_id column
 * @method     ChildUsersFieldsQuery groupByFieldName() Group by the field_name column
 * @method     ChildUsersFieldsQuery groupByFieldValue() Group by the field_value column
 *
 * @method     ChildUsersFieldsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsersFieldsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsersFieldsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsersFieldsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsersFieldsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsersFieldsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsersFieldsQuery leftJoinbundle_UsersFields_Users($relationAlias = null) Adds a LEFT JOIN clause to the query using the bundle_UsersFields_Users relation
 * @method     ChildUsersFieldsQuery rightJoinbundle_UsersFields_Users($relationAlias = null) Adds a RIGHT JOIN clause to the query using the bundle_UsersFields_Users relation
 * @method     ChildUsersFieldsQuery innerJoinbundle_UsersFields_Users($relationAlias = null) Adds a INNER JOIN clause to the query using the bundle_UsersFields_Users relation
 *
 * @method     ChildUsersFieldsQuery joinWithbundle_UsersFields_Users($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the bundle_UsersFields_Users relation
 *
 * @method     ChildUsersFieldsQuery leftJoinWithbundle_UsersFields_Users() Adds a LEFT JOIN clause and with to the query using the bundle_UsersFields_Users relation
 * @method     ChildUsersFieldsQuery rightJoinWithbundle_UsersFields_Users() Adds a RIGHT JOIN clause and with to the query using the bundle_UsersFields_Users relation
 * @method     ChildUsersFieldsQuery innerJoinWithbundle_UsersFields_Users() Adds a INNER JOIN clause and with to the query using the bundle_UsersFields_Users relation
 *
 * @method     \StayOut\DB\UsersQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsersFields findOne(ConnectionInterface $con = null) Return the first ChildUsersFields matching the query
 * @method     ChildUsersFields findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsersFields matching the query, or a new ChildUsersFields object populated from the query conditions when no match is found
 *
 * @method     ChildUsersFields findOneById(string $id) Return the first ChildUsersFields filtered by the id column
 * @method     ChildUsersFields findOneByUserId(string $user_id) Return the first ChildUsersFields filtered by the user_id column
 * @method     ChildUsersFields findOneByFieldName(string $field_name) Return the first ChildUsersFields filtered by the field_name column
 * @method     ChildUsersFields findOneByFieldValue(string $field_value) Return the first ChildUsersFields filtered by the field_value column *

 * @method     ChildUsersFields requirePk($key, ConnectionInterface $con = null) Return the ChildUsersFields by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsersFields requireOne(ConnectionInterface $con = null) Return the first ChildUsersFields matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsersFields requireOneById(string $id) Return the first ChildUsersFields filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsersFields requireOneByUserId(string $user_id) Return the first ChildUsersFields filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsersFields requireOneByFieldName(string $field_name) Return the first ChildUsersFields filtered by the field_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsersFields requireOneByFieldValue(string $field_value) Return the first ChildUsersFields filtered by the field_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsersFields[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsersFields objects based on current ModelCriteria
 * @method     ChildUsersFields[]|ObjectCollection findById(string $id) Return ChildUsersFields objects filtered by the id column
 * @method     ChildUsersFields[]|ObjectCollection findByUserId(string $user_id) Return ChildUsersFields objects filtered by the user_id column
 * @method     ChildUsersFields[]|ObjectCollection findByFieldName(string $field_name) Return ChildUsersFields objects filtered by the field_name column
 * @method     ChildUsersFields[]|ObjectCollection findByFieldValue(string $field_value) Return ChildUsersFields objects filtered by the field_value column
 * @method     ChildUsersFields[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsersFieldsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \StayOut\DB\Base\UsersFieldsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\StayOut\\DB\\UsersFields', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsersFieldsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsersFieldsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsersFieldsQuery) {
            return $criteria;
        }
        $query = new ChildUsersFieldsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsersFields|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsersFieldsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsersFieldsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsersFields A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, field_name, field_value FROM users_fields WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsersFields $obj */
            $obj = new ChildUsersFields();
            $obj->hydrate($row);
            UsersFieldsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsersFields|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsersFieldsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsersFieldsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UsersFieldsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UsersFieldsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersFieldsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterBybundle_UsersFields_Users()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(UsersFieldsTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(UsersFieldsTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersFieldsTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the field_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFieldName('fooValue');   // WHERE field_name = 'fooValue'
     * $query->filterByFieldName('%fooValue%', Criteria::LIKE); // WHERE field_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fieldName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function filterByFieldName($fieldName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fieldName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersFieldsTableMap::COL_FIELD_NAME, $fieldName, $comparison);
    }

    /**
     * Filter the query on the field_value column
     *
     * Example usage:
     * <code>
     * $query->filterByFieldValue('fooValue');   // WHERE field_value = 'fooValue'
     * $query->filterByFieldValue('%fooValue%', Criteria::LIKE); // WHERE field_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fieldValue The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function filterByFieldValue($fieldValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fieldValue)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersFieldsTableMap::COL_FIELD_VALUE, $fieldValue, $comparison);
    }

    /**
     * Filter the query by a related \StayOut\DB\Users object
     *
     * @param \StayOut\DB\Users|ObjectCollection $users The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function filterBybundle_UsersFields_Users($users, $comparison = null)
    {
        if ($users instanceof \StayOut\DB\Users) {
            return $this
                ->addUsingAlias(UsersFieldsTableMap::COL_USER_ID, $users->getId(), $comparison);
        } elseif ($users instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UsersFieldsTableMap::COL_USER_ID, $users->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBybundle_UsersFields_Users() only accepts arguments of type \StayOut\DB\Users or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the bundle_UsersFields_Users relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function joinbundle_UsersFields_Users($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('bundle_UsersFields_Users');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'bundle_UsersFields_Users');
        }

        return $this;
    }

    /**
     * Use the bundle_UsersFields_Users relation Users object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StayOut\DB\UsersQuery A secondary query class using the current class as primary query
     */
    public function usebundle_UsersFields_UsersQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinbundle_UsersFields_Users($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'bundle_UsersFields_Users', '\StayOut\DB\UsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsersFields $usersFields Object to remove from the list of results
     *
     * @return $this|ChildUsersFieldsQuery The current query, for fluid interface
     */
    public function prune($usersFields = null)
    {
        if ($usersFields) {
            $this->addUsingAlias(UsersFieldsTableMap::COL_ID, $usersFields->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the users_fields table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersFieldsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsersFieldsTableMap::clearInstancePool();
            UsersFieldsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersFieldsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsersFieldsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsersFieldsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsersFieldsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsersFieldsQuery
