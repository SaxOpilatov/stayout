<?php

namespace StayOut\DB\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use StayOut\DB\SystemRequests as ChildSystemRequests;
use StayOut\DB\SystemRequestsQuery as ChildSystemRequestsQuery;
use StayOut\DB\Map\SystemRequestsTableMap;

/**
 * Base class that represents a query for the 'system_requests' table.
 *
 *
 *
 * @method     ChildSystemRequestsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSystemRequestsQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildSystemRequestsQuery orderByMethod($order = Criteria::ASC) Order by the method column
 * @method     ChildSystemRequestsQuery orderByHeaders($order = Criteria::ASC) Order by the headers column
 * @method     ChildSystemRequestsQuery orderByGets($order = Criteria::ASC) Order by the gets column
 * @method     ChildSystemRequestsQuery orderByPosts($order = Criteria::ASC) Order by the posts column
 * @method     ChildSystemRequestsQuery orderByInputs($order = Criteria::ASC) Order by the inputs column
 * @method     ChildSystemRequestsQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     ChildSystemRequestsQuery orderByHasErrors($order = Criteria::ASC) Order by the has_errors column
 * @method     ChildSystemRequestsQuery orderByDateCreate($order = Criteria::ASC) Order by the date_create column
 *
 * @method     ChildSystemRequestsQuery groupById() Group by the id column
 * @method     ChildSystemRequestsQuery groupByUrl() Group by the url column
 * @method     ChildSystemRequestsQuery groupByMethod() Group by the method column
 * @method     ChildSystemRequestsQuery groupByHeaders() Group by the headers column
 * @method     ChildSystemRequestsQuery groupByGets() Group by the gets column
 * @method     ChildSystemRequestsQuery groupByPosts() Group by the posts column
 * @method     ChildSystemRequestsQuery groupByInputs() Group by the inputs column
 * @method     ChildSystemRequestsQuery groupByIp() Group by the ip column
 * @method     ChildSystemRequestsQuery groupByHasErrors() Group by the has_errors column
 * @method     ChildSystemRequestsQuery groupByDateCreate() Group by the date_create column
 *
 * @method     ChildSystemRequestsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSystemRequestsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSystemRequestsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSystemRequestsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSystemRequestsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSystemRequestsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSystemRequestsQuery leftJoinSystemErrors($relationAlias = null) Adds a LEFT JOIN clause to the query using the SystemErrors relation
 * @method     ChildSystemRequestsQuery rightJoinSystemErrors($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SystemErrors relation
 * @method     ChildSystemRequestsQuery innerJoinSystemErrors($relationAlias = null) Adds a INNER JOIN clause to the query using the SystemErrors relation
 *
 * @method     ChildSystemRequestsQuery joinWithSystemErrors($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SystemErrors relation
 *
 * @method     ChildSystemRequestsQuery leftJoinWithSystemErrors() Adds a LEFT JOIN clause and with to the query using the SystemErrors relation
 * @method     ChildSystemRequestsQuery rightJoinWithSystemErrors() Adds a RIGHT JOIN clause and with to the query using the SystemErrors relation
 * @method     ChildSystemRequestsQuery innerJoinWithSystemErrors() Adds a INNER JOIN clause and with to the query using the SystemErrors relation
 *
 * @method     \StayOut\DB\SystemErrorsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSystemRequests findOne(ConnectionInterface $con = null) Return the first ChildSystemRequests matching the query
 * @method     ChildSystemRequests findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSystemRequests matching the query, or a new ChildSystemRequests object populated from the query conditions when no match is found
 *
 * @method     ChildSystemRequests findOneById(string $id) Return the first ChildSystemRequests filtered by the id column
 * @method     ChildSystemRequests findOneByUrl(string $url) Return the first ChildSystemRequests filtered by the url column
 * @method     ChildSystemRequests findOneByMethod(string $method) Return the first ChildSystemRequests filtered by the method column
 * @method     ChildSystemRequests findOneByHeaders(string $headers) Return the first ChildSystemRequests filtered by the headers column
 * @method     ChildSystemRequests findOneByGets(string $gets) Return the first ChildSystemRequests filtered by the gets column
 * @method     ChildSystemRequests findOneByPosts(string $posts) Return the first ChildSystemRequests filtered by the posts column
 * @method     ChildSystemRequests findOneByInputs(string $inputs) Return the first ChildSystemRequests filtered by the inputs column
 * @method     ChildSystemRequests findOneByIp(string $ip) Return the first ChildSystemRequests filtered by the ip column
 * @method     ChildSystemRequests findOneByHasErrors(boolean $has_errors) Return the first ChildSystemRequests filtered by the has_errors column
 * @method     ChildSystemRequests findOneByDateCreate(string $date_create) Return the first ChildSystemRequests filtered by the date_create column *

 * @method     ChildSystemRequests requirePk($key, ConnectionInterface $con = null) Return the ChildSystemRequests by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOne(ConnectionInterface $con = null) Return the first ChildSystemRequests matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemRequests requireOneById(string $id) Return the first ChildSystemRequests filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByUrl(string $url) Return the first ChildSystemRequests filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByMethod(string $method) Return the first ChildSystemRequests filtered by the method column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByHeaders(string $headers) Return the first ChildSystemRequests filtered by the headers column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByGets(string $gets) Return the first ChildSystemRequests filtered by the gets column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByPosts(string $posts) Return the first ChildSystemRequests filtered by the posts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByInputs(string $inputs) Return the first ChildSystemRequests filtered by the inputs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByIp(string $ip) Return the first ChildSystemRequests filtered by the ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByHasErrors(boolean $has_errors) Return the first ChildSystemRequests filtered by the has_errors column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemRequests requireOneByDateCreate(string $date_create) Return the first ChildSystemRequests filtered by the date_create column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemRequests[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSystemRequests objects based on current ModelCriteria
 * @method     ChildSystemRequests[]|ObjectCollection findById(string $id) Return ChildSystemRequests objects filtered by the id column
 * @method     ChildSystemRequests[]|ObjectCollection findByUrl(string $url) Return ChildSystemRequests objects filtered by the url column
 * @method     ChildSystemRequests[]|ObjectCollection findByMethod(string $method) Return ChildSystemRequests objects filtered by the method column
 * @method     ChildSystemRequests[]|ObjectCollection findByHeaders(string $headers) Return ChildSystemRequests objects filtered by the headers column
 * @method     ChildSystemRequests[]|ObjectCollection findByGets(string $gets) Return ChildSystemRequests objects filtered by the gets column
 * @method     ChildSystemRequests[]|ObjectCollection findByPosts(string $posts) Return ChildSystemRequests objects filtered by the posts column
 * @method     ChildSystemRequests[]|ObjectCollection findByInputs(string $inputs) Return ChildSystemRequests objects filtered by the inputs column
 * @method     ChildSystemRequests[]|ObjectCollection findByIp(string $ip) Return ChildSystemRequests objects filtered by the ip column
 * @method     ChildSystemRequests[]|ObjectCollection findByHasErrors(boolean $has_errors) Return ChildSystemRequests objects filtered by the has_errors column
 * @method     ChildSystemRequests[]|ObjectCollection findByDateCreate(string $date_create) Return ChildSystemRequests objects filtered by the date_create column
 * @method     ChildSystemRequests[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SystemRequestsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \StayOut\DB\Base\SystemRequestsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\StayOut\\DB\\SystemRequests', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSystemRequestsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSystemRequestsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSystemRequestsQuery) {
            return $criteria;
        }
        $query = new ChildSystemRequestsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSystemRequests|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SystemRequestsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SystemRequestsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSystemRequests A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, url, method, headers, gets, posts, inputs, ip, has_errors, date_create FROM system_requests WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSystemRequests $obj */
            $obj = new ChildSystemRequests();
            $obj->hydrate($row);
            SystemRequestsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSystemRequests|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SystemRequestsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SystemRequestsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SystemRequestsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SystemRequestsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the method column
     *
     * Example usage:
     * <code>
     * $query->filterByMethod('fooValue');   // WHERE method = 'fooValue'
     * $query->filterByMethod('%fooValue%', Criteria::LIKE); // WHERE method LIKE '%fooValue%'
     * </code>
     *
     * @param     string $method The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByMethod($method = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($method)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_METHOD, $method, $comparison);
    }

    /**
     * Filter the query on the headers column
     *
     * Example usage:
     * <code>
     * $query->filterByHeaders('fooValue');   // WHERE headers = 'fooValue'
     * $query->filterByHeaders('%fooValue%', Criteria::LIKE); // WHERE headers LIKE '%fooValue%'
     * </code>
     *
     * @param     string $headers The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByHeaders($headers = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($headers)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_HEADERS, $headers, $comparison);
    }

    /**
     * Filter the query on the gets column
     *
     * Example usage:
     * <code>
     * $query->filterByGets('fooValue');   // WHERE gets = 'fooValue'
     * $query->filterByGets('%fooValue%', Criteria::LIKE); // WHERE gets LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gets The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByGets($gets = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gets)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_GETS, $gets, $comparison);
    }

    /**
     * Filter the query on the posts column
     *
     * Example usage:
     * <code>
     * $query->filterByPosts('fooValue');   // WHERE posts = 'fooValue'
     * $query->filterByPosts('%fooValue%', Criteria::LIKE); // WHERE posts LIKE '%fooValue%'
     * </code>
     *
     * @param     string $posts The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByPosts($posts = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($posts)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_POSTS, $posts, $comparison);
    }

    /**
     * Filter the query on the inputs column
     *
     * Example usage:
     * <code>
     * $query->filterByInputs('fooValue');   // WHERE inputs = 'fooValue'
     * $query->filterByInputs('%fooValue%', Criteria::LIKE); // WHERE inputs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $inputs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByInputs($inputs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($inputs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_INPUTS, $inputs, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%', Criteria::LIKE); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_IP, $ip, $comparison);
    }

    /**
     * Filter the query on the has_errors column
     *
     * Example usage:
     * <code>
     * $query->filterByHasErrors(true); // WHERE has_errors = true
     * $query->filterByHasErrors('yes'); // WHERE has_errors = true
     * </code>
     *
     * @param     boolean|string $hasErrors The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByHasErrors($hasErrors = null, $comparison = null)
    {
        if (is_string($hasErrors)) {
            $hasErrors = in_array(strtolower($hasErrors), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_HAS_ERRORS, $hasErrors, $comparison);
    }

    /**
     * Filter the query on the date_create column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreate('2011-03-14'); // WHERE date_create = '2011-03-14'
     * $query->filterByDateCreate('now'); // WHERE date_create = '2011-03-14'
     * $query->filterByDateCreate(array('max' => 'yesterday')); // WHERE date_create > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterByDateCreate($dateCreate = null, $comparison = null)
    {
        if (is_array($dateCreate)) {
            $useMinMax = false;
            if (isset($dateCreate['min'])) {
                $this->addUsingAlias(SystemRequestsTableMap::COL_DATE_CREATE, $dateCreate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreate['max'])) {
                $this->addUsingAlias(SystemRequestsTableMap::COL_DATE_CREATE, $dateCreate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemRequestsTableMap::COL_DATE_CREATE, $dateCreate, $comparison);
    }

    /**
     * Filter the query by a related \StayOut\DB\SystemErrors object
     *
     * @param \StayOut\DB\SystemErrors|ObjectCollection $systemErrors the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function filterBySystemErrors($systemErrors, $comparison = null)
    {
        if ($systemErrors instanceof \StayOut\DB\SystemErrors) {
            return $this
                ->addUsingAlias(SystemRequestsTableMap::COL_ID, $systemErrors->getRequestId(), $comparison);
        } elseif ($systemErrors instanceof ObjectCollection) {
            return $this
                ->useSystemErrorsQuery()
                ->filterByPrimaryKeys($systemErrors->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySystemErrors() only accepts arguments of type \StayOut\DB\SystemErrors or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SystemErrors relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function joinSystemErrors($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SystemErrors');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SystemErrors');
        }

        return $this;
    }

    /**
     * Use the SystemErrors relation SystemErrors object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StayOut\DB\SystemErrorsQuery A secondary query class using the current class as primary query
     */
    public function useSystemErrorsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSystemErrors($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SystemErrors', '\StayOut\DB\SystemErrorsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSystemRequests $systemRequests Object to remove from the list of results
     *
     * @return $this|ChildSystemRequestsQuery The current query, for fluid interface
     */
    public function prune($systemRequests = null)
    {
        if ($systemRequests) {
            $this->addUsingAlias(SystemRequestsTableMap::COL_ID, $systemRequests->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the system_requests table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemRequestsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SystemRequestsTableMap::clearInstancePool();
            SystemRequestsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemRequestsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SystemRequestsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SystemRequestsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SystemRequestsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SystemRequestsQuery
