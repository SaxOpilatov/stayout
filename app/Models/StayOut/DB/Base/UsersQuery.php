<?php

namespace StayOut\DB\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use StayOut\DB\Users as ChildUsers;
use StayOut\DB\UsersQuery as ChildUsersQuery;
use StayOut\DB\Map\UsersTableMap;

/**
 * Base class that represents a query for the 'users' table.
 *
 *
 *
 * @method     ChildUsersQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUsersQuery orderByHashId($order = Criteria::ASC) Order by the hash_id column
 * @method     ChildUsersQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildUsersQuery orderByLogin($order = Criteria::ASC) Order by the login column
 * @method     ChildUsersQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildUsersQuery orderByIsActivated($order = Criteria::ASC) Order by the is_activated column
 * @method     ChildUsersQuery orderByActivateCode($order = Criteria::ASC) Order by the activate_code column
 * @method     ChildUsersQuery orderByIsDeleted($order = Criteria::ASC) Order by the is_deleted column
 * @method     ChildUsersQuery orderByAuthVk($order = Criteria::ASC) Order by the auth_vk column
 * @method     ChildUsersQuery orderByAuthFb($order = Criteria::ASC) Order by the auth_fb column
 * @method     ChildUsersQuery orderByDateCreate($order = Criteria::ASC) Order by the date_create column
 * @method     ChildUsersQuery orderByDateUpdate($order = Criteria::ASC) Order by the date_update column
 *
 * @method     ChildUsersQuery groupById() Group by the id column
 * @method     ChildUsersQuery groupByHashId() Group by the hash_id column
 * @method     ChildUsersQuery groupByEmail() Group by the email column
 * @method     ChildUsersQuery groupByLogin() Group by the login column
 * @method     ChildUsersQuery groupByPassword() Group by the password column
 * @method     ChildUsersQuery groupByIsActivated() Group by the is_activated column
 * @method     ChildUsersQuery groupByActivateCode() Group by the activate_code column
 * @method     ChildUsersQuery groupByIsDeleted() Group by the is_deleted column
 * @method     ChildUsersQuery groupByAuthVk() Group by the auth_vk column
 * @method     ChildUsersQuery groupByAuthFb() Group by the auth_fb column
 * @method     ChildUsersQuery groupByDateCreate() Group by the date_create column
 * @method     ChildUsersQuery groupByDateUpdate() Group by the date_update column
 *
 * @method     ChildUsersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsersQuery leftJoinUsersTokens($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersTokens relation
 * @method     ChildUsersQuery rightJoinUsersTokens($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersTokens relation
 * @method     ChildUsersQuery innerJoinUsersTokens($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersTokens relation
 *
 * @method     ChildUsersQuery joinWithUsersTokens($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersTokens relation
 *
 * @method     ChildUsersQuery leftJoinWithUsersTokens() Adds a LEFT JOIN clause and with to the query using the UsersTokens relation
 * @method     ChildUsersQuery rightJoinWithUsersTokens() Adds a RIGHT JOIN clause and with to the query using the UsersTokens relation
 * @method     ChildUsersQuery innerJoinWithUsersTokens() Adds a INNER JOIN clause and with to the query using the UsersTokens relation
 *
 * @method     ChildUsersQuery leftJoinUsersRestore($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersRestore relation
 * @method     ChildUsersQuery rightJoinUsersRestore($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersRestore relation
 * @method     ChildUsersQuery innerJoinUsersRestore($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersRestore relation
 *
 * @method     ChildUsersQuery joinWithUsersRestore($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersRestore relation
 *
 * @method     ChildUsersQuery leftJoinWithUsersRestore() Adds a LEFT JOIN clause and with to the query using the UsersRestore relation
 * @method     ChildUsersQuery rightJoinWithUsersRestore() Adds a RIGHT JOIN clause and with to the query using the UsersRestore relation
 * @method     ChildUsersQuery innerJoinWithUsersRestore() Adds a INNER JOIN clause and with to the query using the UsersRestore relation
 *
 * @method     ChildUsersQuery leftJoinUsersFields($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersFields relation
 * @method     ChildUsersQuery rightJoinUsersFields($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersFields relation
 * @method     ChildUsersQuery innerJoinUsersFields($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersFields relation
 *
 * @method     ChildUsersQuery joinWithUsersFields($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersFields relation
 *
 * @method     ChildUsersQuery leftJoinWithUsersFields() Adds a LEFT JOIN clause and with to the query using the UsersFields relation
 * @method     ChildUsersQuery rightJoinWithUsersFields() Adds a RIGHT JOIN clause and with to the query using the UsersFields relation
 * @method     ChildUsersQuery innerJoinWithUsersFields() Adds a INNER JOIN clause and with to the query using the UsersFields relation
 *
 * @method     ChildUsersQuery leftJoinUsersAccess($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsersAccess relation
 * @method     ChildUsersQuery rightJoinUsersAccess($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsersAccess relation
 * @method     ChildUsersQuery innerJoinUsersAccess($relationAlias = null) Adds a INNER JOIN clause to the query using the UsersAccess relation
 *
 * @method     ChildUsersQuery joinWithUsersAccess($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsersAccess relation
 *
 * @method     ChildUsersQuery leftJoinWithUsersAccess() Adds a LEFT JOIN clause and with to the query using the UsersAccess relation
 * @method     ChildUsersQuery rightJoinWithUsersAccess() Adds a RIGHT JOIN clause and with to the query using the UsersAccess relation
 * @method     ChildUsersQuery innerJoinWithUsersAccess() Adds a INNER JOIN clause and with to the query using the UsersAccess relation
 *
 * @method     \StayOut\DB\UsersTokensQuery|\StayOut\DB\UsersRestoreQuery|\StayOut\DB\UsersFieldsQuery|\StayOut\DB\UsersAccessQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsers findOne(ConnectionInterface $con = null) Return the first ChildUsers matching the query
 * @method     ChildUsers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsers matching the query, or a new ChildUsers object populated from the query conditions when no match is found
 *
 * @method     ChildUsers findOneById(string $id) Return the first ChildUsers filtered by the id column
 * @method     ChildUsers findOneByHashId(string $hash_id) Return the first ChildUsers filtered by the hash_id column
 * @method     ChildUsers findOneByEmail(string $email) Return the first ChildUsers filtered by the email column
 * @method     ChildUsers findOneByLogin(string $login) Return the first ChildUsers filtered by the login column
 * @method     ChildUsers findOneByPassword(string $password) Return the first ChildUsers filtered by the password column
 * @method     ChildUsers findOneByIsActivated(boolean $is_activated) Return the first ChildUsers filtered by the is_activated column
 * @method     ChildUsers findOneByActivateCode(string $activate_code) Return the first ChildUsers filtered by the activate_code column
 * @method     ChildUsers findOneByIsDeleted(boolean $is_deleted) Return the first ChildUsers filtered by the is_deleted column
 * @method     ChildUsers findOneByAuthVk(string $auth_vk) Return the first ChildUsers filtered by the auth_vk column
 * @method     ChildUsers findOneByAuthFb(string $auth_fb) Return the first ChildUsers filtered by the auth_fb column
 * @method     ChildUsers findOneByDateCreate(string $date_create) Return the first ChildUsers filtered by the date_create column
 * @method     ChildUsers findOneByDateUpdate(string $date_update) Return the first ChildUsers filtered by the date_update column *

 * @method     ChildUsers requirePk($key, ConnectionInterface $con = null) Return the ChildUsers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOne(ConnectionInterface $con = null) Return the first ChildUsers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsers requireOneById(string $id) Return the first ChildUsers filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByHashId(string $hash_id) Return the first ChildUsers filtered by the hash_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByEmail(string $email) Return the first ChildUsers filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByLogin(string $login) Return the first ChildUsers filtered by the login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByPassword(string $password) Return the first ChildUsers filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByIsActivated(boolean $is_activated) Return the first ChildUsers filtered by the is_activated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByActivateCode(string $activate_code) Return the first ChildUsers filtered by the activate_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByIsDeleted(boolean $is_deleted) Return the first ChildUsers filtered by the is_deleted column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByAuthVk(string $auth_vk) Return the first ChildUsers filtered by the auth_vk column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByAuthFb(string $auth_fb) Return the first ChildUsers filtered by the auth_fb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByDateCreate(string $date_create) Return the first ChildUsers filtered by the date_create column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsers requireOneByDateUpdate(string $date_update) Return the first ChildUsers filtered by the date_update column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsers objects based on current ModelCriteria
 * @method     ChildUsers[]|ObjectCollection findById(string $id) Return ChildUsers objects filtered by the id column
 * @method     ChildUsers[]|ObjectCollection findByHashId(string $hash_id) Return ChildUsers objects filtered by the hash_id column
 * @method     ChildUsers[]|ObjectCollection findByEmail(string $email) Return ChildUsers objects filtered by the email column
 * @method     ChildUsers[]|ObjectCollection findByLogin(string $login) Return ChildUsers objects filtered by the login column
 * @method     ChildUsers[]|ObjectCollection findByPassword(string $password) Return ChildUsers objects filtered by the password column
 * @method     ChildUsers[]|ObjectCollection findByIsActivated(boolean $is_activated) Return ChildUsers objects filtered by the is_activated column
 * @method     ChildUsers[]|ObjectCollection findByActivateCode(string $activate_code) Return ChildUsers objects filtered by the activate_code column
 * @method     ChildUsers[]|ObjectCollection findByIsDeleted(boolean $is_deleted) Return ChildUsers objects filtered by the is_deleted column
 * @method     ChildUsers[]|ObjectCollection findByAuthVk(string $auth_vk) Return ChildUsers objects filtered by the auth_vk column
 * @method     ChildUsers[]|ObjectCollection findByAuthFb(string $auth_fb) Return ChildUsers objects filtered by the auth_fb column
 * @method     ChildUsers[]|ObjectCollection findByDateCreate(string $date_create) Return ChildUsers objects filtered by the date_create column
 * @method     ChildUsers[]|ObjectCollection findByDateUpdate(string $date_update) Return ChildUsers objects filtered by the date_update column
 * @method     ChildUsers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \StayOut\DB\Base\UsersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\StayOut\\DB\\Users', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsersQuery) {
            return $criteria;
        }
        $query = new ChildUsersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, hash_id, email, login, password, is_activated, activate_code, is_deleted, auth_vk, auth_fb, date_create, date_update FROM users WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsers $obj */
            $obj = new ChildUsers();
            $obj->hydrate($row);
            UsersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UsersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UsersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the hash_id column
     *
     * Example usage:
     * <code>
     * $query->filterByHashId('fooValue');   // WHERE hash_id = 'fooValue'
     * $query->filterByHashId('%fooValue%', Criteria::LIKE); // WHERE hash_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hashId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByHashId($hashId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hashId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_HASH_ID, $hashId, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the login column
     *
     * Example usage:
     * <code>
     * $query->filterByLogin('fooValue');   // WHERE login = 'fooValue'
     * $query->filterByLogin('%fooValue%', Criteria::LIKE); // WHERE login LIKE '%fooValue%'
     * </code>
     *
     * @param     string $login The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByLogin($login = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($login)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_LOGIN, $login, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the is_activated column
     *
     * Example usage:
     * <code>
     * $query->filterByIsActivated(true); // WHERE is_activated = true
     * $query->filterByIsActivated('yes'); // WHERE is_activated = true
     * </code>
     *
     * @param     boolean|string $isActivated The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByIsActivated($isActivated = null, $comparison = null)
    {
        if (is_string($isActivated)) {
            $isActivated = in_array(strtolower($isActivated), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UsersTableMap::COL_IS_ACTIVATED, $isActivated, $comparison);
    }

    /**
     * Filter the query on the activate_code column
     *
     * Example usage:
     * <code>
     * $query->filterByActivateCode(1234); // WHERE activate_code = 1234
     * $query->filterByActivateCode(array(12, 34)); // WHERE activate_code IN (12, 34)
     * $query->filterByActivateCode(array('min' => 12)); // WHERE activate_code > 12
     * </code>
     *
     * @param     mixed $activateCode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByActivateCode($activateCode = null, $comparison = null)
    {
        if (is_array($activateCode)) {
            $useMinMax = false;
            if (isset($activateCode['min'])) {
                $this->addUsingAlias(UsersTableMap::COL_ACTIVATE_CODE, $activateCode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activateCode['max'])) {
                $this->addUsingAlias(UsersTableMap::COL_ACTIVATE_CODE, $activateCode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_ACTIVATE_CODE, $activateCode, $comparison);
    }

    /**
     * Filter the query on the is_deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDeleted(true); // WHERE is_deleted = true
     * $query->filterByIsDeleted('yes'); // WHERE is_deleted = true
     * </code>
     *
     * @param     boolean|string $isDeleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByIsDeleted($isDeleted = null, $comparison = null)
    {
        if (is_string($isDeleted)) {
            $isDeleted = in_array(strtolower($isDeleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UsersTableMap::COL_IS_DELETED, $isDeleted, $comparison);
    }

    /**
     * Filter the query on the auth_vk column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthVk('fooValue');   // WHERE auth_vk = 'fooValue'
     * $query->filterByAuthVk('%fooValue%', Criteria::LIKE); // WHERE auth_vk LIKE '%fooValue%'
     * </code>
     *
     * @param     string $authVk The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByAuthVk($authVk = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($authVk)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_AUTH_VK, $authVk, $comparison);
    }

    /**
     * Filter the query on the auth_fb column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthFb('fooValue');   // WHERE auth_fb = 'fooValue'
     * $query->filterByAuthFb('%fooValue%', Criteria::LIKE); // WHERE auth_fb LIKE '%fooValue%'
     * </code>
     *
     * @param     string $authFb The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByAuthFb($authFb = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($authFb)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_AUTH_FB, $authFb, $comparison);
    }

    /**
     * Filter the query on the date_create column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreate('2011-03-14'); // WHERE date_create = '2011-03-14'
     * $query->filterByDateCreate('now'); // WHERE date_create = '2011-03-14'
     * $query->filterByDateCreate(array('max' => 'yesterday')); // WHERE date_create > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByDateCreate($dateCreate = null, $comparison = null)
    {
        if (is_array($dateCreate)) {
            $useMinMax = false;
            if (isset($dateCreate['min'])) {
                $this->addUsingAlias(UsersTableMap::COL_DATE_CREATE, $dateCreate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreate['max'])) {
                $this->addUsingAlias(UsersTableMap::COL_DATE_CREATE, $dateCreate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_DATE_CREATE, $dateCreate, $comparison);
    }

    /**
     * Filter the query on the date_update column
     *
     * Example usage:
     * <code>
     * $query->filterByDateUpdate('2011-03-14'); // WHERE date_update = '2011-03-14'
     * $query->filterByDateUpdate('now'); // WHERE date_update = '2011-03-14'
     * $query->filterByDateUpdate(array('max' => 'yesterday')); // WHERE date_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function filterByDateUpdate($dateUpdate = null, $comparison = null)
    {
        if (is_array($dateUpdate)) {
            $useMinMax = false;
            if (isset($dateUpdate['min'])) {
                $this->addUsingAlias(UsersTableMap::COL_DATE_UPDATE, $dateUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateUpdate['max'])) {
                $this->addUsingAlias(UsersTableMap::COL_DATE_UPDATE, $dateUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsersTableMap::COL_DATE_UPDATE, $dateUpdate, $comparison);
    }

    /**
     * Filter the query by a related \StayOut\DB\UsersTokens object
     *
     * @param \StayOut\DB\UsersTokens|ObjectCollection $usersTokens the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUsersTokens($usersTokens, $comparison = null)
    {
        if ($usersTokens instanceof \StayOut\DB\UsersTokens) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $usersTokens->getUserId(), $comparison);
        } elseif ($usersTokens instanceof ObjectCollection) {
            return $this
                ->useUsersTokensQuery()
                ->filterByPrimaryKeys($usersTokens->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsersTokens() only accepts arguments of type \StayOut\DB\UsersTokens or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersTokens relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinUsersTokens($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersTokens');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersTokens');
        }

        return $this;
    }

    /**
     * Use the UsersTokens relation UsersTokens object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StayOut\DB\UsersTokensQuery A secondary query class using the current class as primary query
     */
    public function useUsersTokensQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsersTokens($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersTokens', '\StayOut\DB\UsersTokensQuery');
    }

    /**
     * Filter the query by a related \StayOut\DB\UsersRestore object
     *
     * @param \StayOut\DB\UsersRestore|ObjectCollection $usersRestore the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUsersRestore($usersRestore, $comparison = null)
    {
        if ($usersRestore instanceof \StayOut\DB\UsersRestore) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $usersRestore->getUserId(), $comparison);
        } elseif ($usersRestore instanceof ObjectCollection) {
            return $this
                ->useUsersRestoreQuery()
                ->filterByPrimaryKeys($usersRestore->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsersRestore() only accepts arguments of type \StayOut\DB\UsersRestore or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersRestore relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinUsersRestore($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersRestore');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersRestore');
        }

        return $this;
    }

    /**
     * Use the UsersRestore relation UsersRestore object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StayOut\DB\UsersRestoreQuery A secondary query class using the current class as primary query
     */
    public function useUsersRestoreQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsersRestore($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersRestore', '\StayOut\DB\UsersRestoreQuery');
    }

    /**
     * Filter the query by a related \StayOut\DB\UsersFields object
     *
     * @param \StayOut\DB\UsersFields|ObjectCollection $usersFields the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUsersFields($usersFields, $comparison = null)
    {
        if ($usersFields instanceof \StayOut\DB\UsersFields) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $usersFields->getUserId(), $comparison);
        } elseif ($usersFields instanceof ObjectCollection) {
            return $this
                ->useUsersFieldsQuery()
                ->filterByPrimaryKeys($usersFields->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsersFields() only accepts arguments of type \StayOut\DB\UsersFields or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersFields relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinUsersFields($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersFields');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersFields');
        }

        return $this;
    }

    /**
     * Use the UsersFields relation UsersFields object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StayOut\DB\UsersFieldsQuery A secondary query class using the current class as primary query
     */
    public function useUsersFieldsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsersFields($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersFields', '\StayOut\DB\UsersFieldsQuery');
    }

    /**
     * Filter the query by a related \StayOut\DB\UsersAccess object
     *
     * @param \StayOut\DB\UsersAccess|ObjectCollection $usersAccess the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsersQuery The current query, for fluid interface
     */
    public function filterByUsersAccess($usersAccess, $comparison = null)
    {
        if ($usersAccess instanceof \StayOut\DB\UsersAccess) {
            return $this
                ->addUsingAlias(UsersTableMap::COL_ID, $usersAccess->getUserId(), $comparison);
        } elseif ($usersAccess instanceof ObjectCollection) {
            return $this
                ->useUsersAccessQuery()
                ->filterByPrimaryKeys($usersAccess->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsersAccess() only accepts arguments of type \StayOut\DB\UsersAccess or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsersAccess relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function joinUsersAccess($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsersAccess');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsersAccess');
        }

        return $this;
    }

    /**
     * Use the UsersAccess relation UsersAccess object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \StayOut\DB\UsersAccessQuery A secondary query class using the current class as primary query
     */
    public function useUsersAccessQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsersAccess($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsersAccess', '\StayOut\DB\UsersAccessQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsers $users Object to remove from the list of results
     *
     * @return $this|ChildUsersQuery The current query, for fluid interface
     */
    public function prune($users = null)
    {
        if ($users) {
            $this->addUsingAlias(UsersTableMap::COL_ID, $users->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsersTableMap::clearInstancePool();
            UsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsersQuery
