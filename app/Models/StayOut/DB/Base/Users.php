<?php

namespace StayOut\DB\Base;

use \DateTime;
use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;
use StayOut\DB\Users as ChildUsers;
use StayOut\DB\UsersAccess as ChildUsersAccess;
use StayOut\DB\UsersAccessQuery as ChildUsersAccessQuery;
use StayOut\DB\UsersFields as ChildUsersFields;
use StayOut\DB\UsersFieldsQuery as ChildUsersFieldsQuery;
use StayOut\DB\UsersQuery as ChildUsersQuery;
use StayOut\DB\UsersRestore as ChildUsersRestore;
use StayOut\DB\UsersRestoreQuery as ChildUsersRestoreQuery;
use StayOut\DB\UsersTokens as ChildUsersTokens;
use StayOut\DB\UsersTokensQuery as ChildUsersTokensQuery;
use StayOut\DB\Map\UsersAccessTableMap;
use StayOut\DB\Map\UsersFieldsTableMap;
use StayOut\DB\Map\UsersRestoreTableMap;
use StayOut\DB\Map\UsersTableMap;
use StayOut\DB\Map\UsersTokensTableMap;

/**
 * Base class that represents a row from the 'users' table.
 *
 *
 *
 * @package    propel.generator.StayOut.DB.Base
 */
abstract class Users implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\StayOut\\DB\\Map\\UsersTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the hash_id field.
     *
     * @var        string
     */
    protected $hash_id;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the login field.
     *
     * @var        string
     */
    protected $login;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the is_activated field.
     *
     * @var        boolean
     */
    protected $is_activated;

    /**
     * The value for the activate_code field.
     *
     * @var        string
     */
    protected $activate_code;

    /**
     * The value for the is_deleted field.
     *
     * @var        boolean
     */
    protected $is_deleted;

    /**
     * The value for the auth_vk field.
     *
     * @var        string
     */
    protected $auth_vk;

    /**
     * The value for the auth_fb field.
     *
     * @var        string
     */
    protected $auth_fb;

    /**
     * The value for the date_create field.
     *
     * @var        DateTime
     */
    protected $date_create;

    /**
     * The value for the date_update field.
     *
     * @var        DateTime
     */
    protected $date_update;

    /**
     * @var        ObjectCollection|ChildUsersTokens[] Collection to store aggregation of ChildUsersTokens objects.
     */
    protected $collUsersTokenss;
    protected $collUsersTokenssPartial;

    /**
     * @var        ObjectCollection|ChildUsersRestore[] Collection to store aggregation of ChildUsersRestore objects.
     */
    protected $collUsersRestores;
    protected $collUsersRestoresPartial;

    /**
     * @var        ObjectCollection|ChildUsersFields[] Collection to store aggregation of ChildUsersFields objects.
     */
    protected $collUsersFieldss;
    protected $collUsersFieldssPartial;

    /**
     * @var        ObjectCollection|ChildUsersAccess[] Collection to store aggregation of ChildUsersAccess objects.
     */
    protected $collUsersAccesses;
    protected $collUsersAccessesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsersTokens[]
     */
    protected $usersTokenssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsersRestore[]
     */
    protected $usersRestoresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsersFields[]
     */
    protected $usersFieldssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsersAccess[]
     */
    protected $usersAccessesScheduledForDeletion = null;

    /**
     * Initializes internal state of StayOut\DB\Base\Users object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Users</code> instance.  If
     * <code>obj</code> is an instance of <code>Users</code>, delegates to
     * <code>equals(Users)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Users The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [hash_id] column value.
     *
     * @return string
     */
    public function getHashId()
    {
        return $this->hash_id;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [login] column value.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [is_activated] column value.
     *
     * @return boolean
     */
    public function getIsActivated()
    {
        return $this->is_activated;
    }

    /**
     * Get the [is_activated] column value.
     *
     * @return boolean
     */
    public function isActivated()
    {
        return $this->getIsActivated();
    }

    /**
     * Get the [activate_code] column value.
     *
     * @return string
     */
    public function getActivateCode()
    {
        return $this->activate_code;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Get the [auth_vk] column value.
     *
     * @return string
     */
    public function getAuthVk()
    {
        return $this->auth_vk;
    }

    /**
     * Get the [auth_fb] column value.
     *
     * @return string
     */
    public function getAuthFb()
    {
        return $this->auth_fb;
    }

    /**
     * Get the [optionally formatted] temporal [date_create] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreate($format = NULL)
    {
        if ($format === null) {
            return $this->date_create;
        } else {
            return $this->date_create instanceof \DateTimeInterface ? $this->date_create->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [date_update] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateUpdate($format = NULL)
    {
        if ($format === null) {
            return $this->date_update;
        } else {
            return $this->date_update instanceof \DateTimeInterface ? $this->date_update->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UsersTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [hash_id] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setHashId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hash_id !== $v) {
            $this->hash_id = $v;
            $this->modifiedColumns[UsersTableMap::COL_HASH_ID] = true;
        }

        return $this;
    } // setHashId()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[UsersTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [login] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[UsersTableMap::COL_LOGIN] = true;
        }

        return $this;
    } // setLogin()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[UsersTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Sets the value of the [is_activated] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setIsActivated($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_activated !== $v) {
            $this->is_activated = $v;
            $this->modifiedColumns[UsersTableMap::COL_IS_ACTIVATED] = true;
        }

        return $this;
    } // setIsActivated()

    /**
     * Set the value of [activate_code] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setActivateCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->activate_code !== $v) {
            $this->activate_code = $v;
            $this->modifiedColumns[UsersTableMap::COL_ACTIVATE_CODE] = true;
        }

        return $this;
    } // setActivateCode()

    /**
     * Sets the value of the [is_deleted] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setIsDeleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deleted !== $v) {
            $this->is_deleted = $v;
            $this->modifiedColumns[UsersTableMap::COL_IS_DELETED] = true;
        }

        return $this;
    } // setIsDeleted()

    /**
     * Set the value of [auth_vk] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setAuthVk($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->auth_vk !== $v) {
            $this->auth_vk = $v;
            $this->modifiedColumns[UsersTableMap::COL_AUTH_VK] = true;
        }

        return $this;
    } // setAuthVk()

    /**
     * Set the value of [auth_fb] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setAuthFb($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->auth_fb !== $v) {
            $this->auth_fb = $v;
            $this->modifiedColumns[UsersTableMap::COL_AUTH_FB] = true;
        }

        return $this;
    } // setAuthFb()

    /**
     * Sets the value of [date_create] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setDateCreate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_create !== null || $dt !== null) {
            if ($this->date_create === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_create->format("Y-m-d H:i:s.u")) {
                $this->date_create = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UsersTableMap::COL_DATE_CREATE] = true;
            }
        } // if either are not null

        return $this;
    } // setDateCreate()

    /**
     * Sets the value of [date_update] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function setDateUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_update !== null || $dt !== null) {
            if ($this->date_update === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_update->format("Y-m-d H:i:s.u")) {
                $this->date_update = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UsersTableMap::COL_DATE_UPDATE] = true;
            }
        } // if either are not null

        return $this;
    } // setDateUpdate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UsersTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UsersTableMap::translateFieldName('HashId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->hash_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UsersTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UsersTableMap::translateFieldName('Login', TableMap::TYPE_PHPNAME, $indexType)];
            $this->login = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UsersTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UsersTableMap::translateFieldName('IsActivated', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_activated = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UsersTableMap::translateFieldName('ActivateCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->activate_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UsersTableMap::translateFieldName('IsDeleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deleted = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UsersTableMap::translateFieldName('AuthVk', TableMap::TYPE_PHPNAME, $indexType)];
            $this->auth_vk = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UsersTableMap::translateFieldName('AuthFb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->auth_fb = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UsersTableMap::translateFieldName('DateCreate', TableMap::TYPE_PHPNAME, $indexType)];
            $this->date_create = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UsersTableMap::translateFieldName('DateUpdate', TableMap::TYPE_PHPNAME, $indexType)];
            $this->date_update = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = UsersTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\StayOut\\DB\\Users'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsersTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUsersQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collUsersTokenss = null;

            $this->collUsersRestores = null;

            $this->collUsersFieldss = null;

            $this->collUsersAccesses = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Users::setDeleted()
     * @see Users::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUsersQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UsersTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->usersTokenssScheduledForDeletion !== null) {
                if (!$this->usersTokenssScheduledForDeletion->isEmpty()) {
                    \StayOut\DB\UsersTokensQuery::create()
                        ->filterByPrimaryKeys($this->usersTokenssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usersTokenssScheduledForDeletion = null;
                }
            }

            if ($this->collUsersTokenss !== null) {
                foreach ($this->collUsersTokenss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usersRestoresScheduledForDeletion !== null) {
                if (!$this->usersRestoresScheduledForDeletion->isEmpty()) {
                    \StayOut\DB\UsersRestoreQuery::create()
                        ->filterByPrimaryKeys($this->usersRestoresScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usersRestoresScheduledForDeletion = null;
                }
            }

            if ($this->collUsersRestores !== null) {
                foreach ($this->collUsersRestores as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usersFieldssScheduledForDeletion !== null) {
                if (!$this->usersFieldssScheduledForDeletion->isEmpty()) {
                    \StayOut\DB\UsersFieldsQuery::create()
                        ->filterByPrimaryKeys($this->usersFieldssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usersFieldssScheduledForDeletion = null;
                }
            }

            if ($this->collUsersFieldss !== null) {
                foreach ($this->collUsersFieldss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usersAccessesScheduledForDeletion !== null) {
                if (!$this->usersAccessesScheduledForDeletion->isEmpty()) {
                    \StayOut\DB\UsersAccessQuery::create()
                        ->filterByPrimaryKeys($this->usersAccessesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usersAccessesScheduledForDeletion = null;
                }
            }

            if ($this->collUsersAccesses !== null) {
                foreach ($this->collUsersAccesses as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UsersTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UsersTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('users_id_seq')");
                $this->id = (string) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UsersTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UsersTableMap::COL_HASH_ID)) {
            $modifiedColumns[':p' . $index++]  = 'hash_id';
        }
        if ($this->isColumnModified(UsersTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(UsersTableMap::COL_LOGIN)) {
            $modifiedColumns[':p' . $index++]  = 'login';
        }
        if ($this->isColumnModified(UsersTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(UsersTableMap::COL_IS_ACTIVATED)) {
            $modifiedColumns[':p' . $index++]  = 'is_activated';
        }
        if ($this->isColumnModified(UsersTableMap::COL_ACTIVATE_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'activate_code';
        }
        if ($this->isColumnModified(UsersTableMap::COL_IS_DELETED)) {
            $modifiedColumns[':p' . $index++]  = 'is_deleted';
        }
        if ($this->isColumnModified(UsersTableMap::COL_AUTH_VK)) {
            $modifiedColumns[':p' . $index++]  = 'auth_vk';
        }
        if ($this->isColumnModified(UsersTableMap::COL_AUTH_FB)) {
            $modifiedColumns[':p' . $index++]  = 'auth_fb';
        }
        if ($this->isColumnModified(UsersTableMap::COL_DATE_CREATE)) {
            $modifiedColumns[':p' . $index++]  = 'date_create';
        }
        if ($this->isColumnModified(UsersTableMap::COL_DATE_UPDATE)) {
            $modifiedColumns[':p' . $index++]  = 'date_update';
        }

        $sql = sprintf(
            'INSERT INTO users (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'hash_id':
                        $stmt->bindValue($identifier, $this->hash_id, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'login':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'is_activated':
                        $stmt->bindValue($identifier, $this->is_activated, PDO::PARAM_BOOL);
                        break;
                    case 'activate_code':
                        $stmt->bindValue($identifier, $this->activate_code, PDO::PARAM_INT);
                        break;
                    case 'is_deleted':
                        $stmt->bindValue($identifier, $this->is_deleted, PDO::PARAM_BOOL);
                        break;
                    case 'auth_vk':
                        $stmt->bindValue($identifier, $this->auth_vk, PDO::PARAM_STR);
                        break;
                    case 'auth_fb':
                        $stmt->bindValue($identifier, $this->auth_fb, PDO::PARAM_STR);
                        break;
                    case 'date_create':
                        $stmt->bindValue($identifier, $this->date_create ? $this->date_create->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'date_update':
                        $stmt->bindValue($identifier, $this->date_update ? $this->date_update->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getHashId();
                break;
            case 2:
                return $this->getEmail();
                break;
            case 3:
                return $this->getLogin();
                break;
            case 4:
                return $this->getPassword();
                break;
            case 5:
                return $this->getIsActivated();
                break;
            case 6:
                return $this->getActivateCode();
                break;
            case 7:
                return $this->getIsDeleted();
                break;
            case 8:
                return $this->getAuthVk();
                break;
            case 9:
                return $this->getAuthFb();
                break;
            case 10:
                return $this->getDateCreate();
                break;
            case 11:
                return $this->getDateUpdate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Users'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Users'][$this->hashCode()] = true;
        $keys = UsersTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getHashId(),
            $keys[2] => $this->getEmail(),
            $keys[3] => $this->getLogin(),
            $keys[4] => $this->getPassword(),
            $keys[5] => $this->getIsActivated(),
            $keys[6] => $this->getActivateCode(),
            $keys[7] => $this->getIsDeleted(),
            $keys[8] => $this->getAuthVk(),
            $keys[9] => $this->getAuthFb(),
            $keys[10] => $this->getDateCreate(),
            $keys[11] => $this->getDateUpdate(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collUsersTokenss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usersTokenss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users_tokenss';
                        break;
                    default:
                        $key = 'UsersTokenss';
                }

                $result[$key] = $this->collUsersTokenss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsersRestores) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usersRestores';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users_restores';
                        break;
                    default:
                        $key = 'UsersRestores';
                }

                $result[$key] = $this->collUsersRestores->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsersFieldss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usersFieldss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users_fieldss';
                        break;
                    default:
                        $key = 'UsersFieldss';
                }

                $result[$key] = $this->collUsersFieldss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsersAccesses) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usersAccesses';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users_accesses';
                        break;
                    default:
                        $key = 'UsersAccesses';
                }

                $result[$key] = $this->collUsersAccesses->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\StayOut\DB\Users
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\StayOut\DB\Users
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setHashId($value);
                break;
            case 2:
                $this->setEmail($value);
                break;
            case 3:
                $this->setLogin($value);
                break;
            case 4:
                $this->setPassword($value);
                break;
            case 5:
                $this->setIsActivated($value);
                break;
            case 6:
                $this->setActivateCode($value);
                break;
            case 7:
                $this->setIsDeleted($value);
                break;
            case 8:
                $this->setAuthVk($value);
                break;
            case 9:
                $this->setAuthFb($value);
                break;
            case 10:
                $this->setDateCreate($value);
                break;
            case 11:
                $this->setDateUpdate($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UsersTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setHashId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setEmail($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setLogin($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPassword($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIsActivated($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setActivateCode($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIsDeleted($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setAuthVk($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setAuthFb($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDateCreate($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setDateUpdate($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\StayOut\DB\Users The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UsersTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UsersTableMap::COL_ID)) {
            $criteria->add(UsersTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UsersTableMap::COL_HASH_ID)) {
            $criteria->add(UsersTableMap::COL_HASH_ID, $this->hash_id);
        }
        if ($this->isColumnModified(UsersTableMap::COL_EMAIL)) {
            $criteria->add(UsersTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(UsersTableMap::COL_LOGIN)) {
            $criteria->add(UsersTableMap::COL_LOGIN, $this->login);
        }
        if ($this->isColumnModified(UsersTableMap::COL_PASSWORD)) {
            $criteria->add(UsersTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(UsersTableMap::COL_IS_ACTIVATED)) {
            $criteria->add(UsersTableMap::COL_IS_ACTIVATED, $this->is_activated);
        }
        if ($this->isColumnModified(UsersTableMap::COL_ACTIVATE_CODE)) {
            $criteria->add(UsersTableMap::COL_ACTIVATE_CODE, $this->activate_code);
        }
        if ($this->isColumnModified(UsersTableMap::COL_IS_DELETED)) {
            $criteria->add(UsersTableMap::COL_IS_DELETED, $this->is_deleted);
        }
        if ($this->isColumnModified(UsersTableMap::COL_AUTH_VK)) {
            $criteria->add(UsersTableMap::COL_AUTH_VK, $this->auth_vk);
        }
        if ($this->isColumnModified(UsersTableMap::COL_AUTH_FB)) {
            $criteria->add(UsersTableMap::COL_AUTH_FB, $this->auth_fb);
        }
        if ($this->isColumnModified(UsersTableMap::COL_DATE_CREATE)) {
            $criteria->add(UsersTableMap::COL_DATE_CREATE, $this->date_create);
        }
        if ($this->isColumnModified(UsersTableMap::COL_DATE_UPDATE)) {
            $criteria->add(UsersTableMap::COL_DATE_UPDATE, $this->date_update);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUsersQuery::create();
        $criteria->add(UsersTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \StayOut\DB\Users (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setHashId($this->getHashId());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setLogin($this->getLogin());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setIsActivated($this->getIsActivated());
        $copyObj->setActivateCode($this->getActivateCode());
        $copyObj->setIsDeleted($this->getIsDeleted());
        $copyObj->setAuthVk($this->getAuthVk());
        $copyObj->setAuthFb($this->getAuthFb());
        $copyObj->setDateCreate($this->getDateCreate());
        $copyObj->setDateUpdate($this->getDateUpdate());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUsersTokenss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsersTokens($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsersRestores() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsersRestore($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsersFieldss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsersFields($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsersAccesses() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsersAccess($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \StayOut\DB\Users Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UsersTokens' == $relationName) {
            $this->initUsersTokenss();
            return;
        }
        if ('UsersRestore' == $relationName) {
            $this->initUsersRestores();
            return;
        }
        if ('UsersFields' == $relationName) {
            $this->initUsersFieldss();
            return;
        }
        if ('UsersAccess' == $relationName) {
            $this->initUsersAccesses();
            return;
        }
    }

    /**
     * Clears out the collUsersTokenss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsersTokenss()
     */
    public function clearUsersTokenss()
    {
        $this->collUsersTokenss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsersTokenss collection loaded partially.
     */
    public function resetPartialUsersTokenss($v = true)
    {
        $this->collUsersTokenssPartial = $v;
    }

    /**
     * Initializes the collUsersTokenss collection.
     *
     * By default this just sets the collUsersTokenss collection to an empty array (like clearcollUsersTokenss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsersTokenss($overrideExisting = true)
    {
        if (null !== $this->collUsersTokenss && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsersTokensTableMap::getTableMap()->getCollectionClassName();

        $this->collUsersTokenss = new $collectionClassName;
        $this->collUsersTokenss->setModel('\StayOut\DB\UsersTokens');
    }

    /**
     * Gets an array of ChildUsersTokens objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsersTokens[] List of ChildUsersTokens objects
     * @throws PropelException
     */
    public function getUsersTokenss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersTokenssPartial && !$this->isNew();
        if (null === $this->collUsersTokenss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUsersTokenss) {
                // return empty collection
                $this->initUsersTokenss();
            } else {
                $collUsersTokenss = ChildUsersTokensQuery::create(null, $criteria)
                    ->filterBybundle_UsersTokens_Users($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsersTokenssPartial && count($collUsersTokenss)) {
                        $this->initUsersTokenss(false);

                        foreach ($collUsersTokenss as $obj) {
                            if (false == $this->collUsersTokenss->contains($obj)) {
                                $this->collUsersTokenss->append($obj);
                            }
                        }

                        $this->collUsersTokenssPartial = true;
                    }

                    return $collUsersTokenss;
                }

                if ($partial && $this->collUsersTokenss) {
                    foreach ($this->collUsersTokenss as $obj) {
                        if ($obj->isNew()) {
                            $collUsersTokenss[] = $obj;
                        }
                    }
                }

                $this->collUsersTokenss = $collUsersTokenss;
                $this->collUsersTokenssPartial = false;
            }
        }

        return $this->collUsersTokenss;
    }

    /**
     * Sets a collection of ChildUsersTokens objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usersTokenss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setUsersTokenss(Collection $usersTokenss, ConnectionInterface $con = null)
    {
        /** @var ChildUsersTokens[] $usersTokenssToDelete */
        $usersTokenssToDelete = $this->getUsersTokenss(new Criteria(), $con)->diff($usersTokenss);


        $this->usersTokenssScheduledForDeletion = $usersTokenssToDelete;

        foreach ($usersTokenssToDelete as $usersTokensRemoved) {
            $usersTokensRemoved->setbundle_UsersTokens_Users(null);
        }

        $this->collUsersTokenss = null;
        foreach ($usersTokenss as $usersTokens) {
            $this->addUsersTokens($usersTokens);
        }

        $this->collUsersTokenss = $usersTokenss;
        $this->collUsersTokenssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsersTokens objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsersTokens objects.
     * @throws PropelException
     */
    public function countUsersTokenss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersTokenssPartial && !$this->isNew();
        if (null === $this->collUsersTokenss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsersTokenss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsersTokenss());
            }

            $query = ChildUsersTokensQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBybundle_UsersTokens_Users($this)
                ->count($con);
        }

        return count($this->collUsersTokenss);
    }

    /**
     * Method called to associate a ChildUsersTokens object to this object
     * through the ChildUsersTokens foreign key attribute.
     *
     * @param  ChildUsersTokens $l ChildUsersTokens
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function addUsersTokens(ChildUsersTokens $l)
    {
        if ($this->collUsersTokenss === null) {
            $this->initUsersTokenss();
            $this->collUsersTokenssPartial = true;
        }

        if (!$this->collUsersTokenss->contains($l)) {
            $this->doAddUsersTokens($l);

            if ($this->usersTokenssScheduledForDeletion and $this->usersTokenssScheduledForDeletion->contains($l)) {
                $this->usersTokenssScheduledForDeletion->remove($this->usersTokenssScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsersTokens $usersTokens The ChildUsersTokens object to add.
     */
    protected function doAddUsersTokens(ChildUsersTokens $usersTokens)
    {
        $this->collUsersTokenss[]= $usersTokens;
        $usersTokens->setbundle_UsersTokens_Users($this);
    }

    /**
     * @param  ChildUsersTokens $usersTokens The ChildUsersTokens object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeUsersTokens(ChildUsersTokens $usersTokens)
    {
        if ($this->getUsersTokenss()->contains($usersTokens)) {
            $pos = $this->collUsersTokenss->search($usersTokens);
            $this->collUsersTokenss->remove($pos);
            if (null === $this->usersTokenssScheduledForDeletion) {
                $this->usersTokenssScheduledForDeletion = clone $this->collUsersTokenss;
                $this->usersTokenssScheduledForDeletion->clear();
            }
            $this->usersTokenssScheduledForDeletion[]= clone $usersTokens;
            $usersTokens->setbundle_UsersTokens_Users(null);
        }

        return $this;
    }

    /**
     * Clears out the collUsersRestores collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsersRestores()
     */
    public function clearUsersRestores()
    {
        $this->collUsersRestores = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsersRestores collection loaded partially.
     */
    public function resetPartialUsersRestores($v = true)
    {
        $this->collUsersRestoresPartial = $v;
    }

    /**
     * Initializes the collUsersRestores collection.
     *
     * By default this just sets the collUsersRestores collection to an empty array (like clearcollUsersRestores());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsersRestores($overrideExisting = true)
    {
        if (null !== $this->collUsersRestores && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsersRestoreTableMap::getTableMap()->getCollectionClassName();

        $this->collUsersRestores = new $collectionClassName;
        $this->collUsersRestores->setModel('\StayOut\DB\UsersRestore');
    }

    /**
     * Gets an array of ChildUsersRestore objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsersRestore[] List of ChildUsersRestore objects
     * @throws PropelException
     */
    public function getUsersRestores(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersRestoresPartial && !$this->isNew();
        if (null === $this->collUsersRestores || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUsersRestores) {
                // return empty collection
                $this->initUsersRestores();
            } else {
                $collUsersRestores = ChildUsersRestoreQuery::create(null, $criteria)
                    ->filterBybundle_UsersRestore_Users($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsersRestoresPartial && count($collUsersRestores)) {
                        $this->initUsersRestores(false);

                        foreach ($collUsersRestores as $obj) {
                            if (false == $this->collUsersRestores->contains($obj)) {
                                $this->collUsersRestores->append($obj);
                            }
                        }

                        $this->collUsersRestoresPartial = true;
                    }

                    return $collUsersRestores;
                }

                if ($partial && $this->collUsersRestores) {
                    foreach ($this->collUsersRestores as $obj) {
                        if ($obj->isNew()) {
                            $collUsersRestores[] = $obj;
                        }
                    }
                }

                $this->collUsersRestores = $collUsersRestores;
                $this->collUsersRestoresPartial = false;
            }
        }

        return $this->collUsersRestores;
    }

    /**
     * Sets a collection of ChildUsersRestore objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usersRestores A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setUsersRestores(Collection $usersRestores, ConnectionInterface $con = null)
    {
        /** @var ChildUsersRestore[] $usersRestoresToDelete */
        $usersRestoresToDelete = $this->getUsersRestores(new Criteria(), $con)->diff($usersRestores);


        $this->usersRestoresScheduledForDeletion = $usersRestoresToDelete;

        foreach ($usersRestoresToDelete as $usersRestoreRemoved) {
            $usersRestoreRemoved->setbundle_UsersRestore_Users(null);
        }

        $this->collUsersRestores = null;
        foreach ($usersRestores as $usersRestore) {
            $this->addUsersRestore($usersRestore);
        }

        $this->collUsersRestores = $usersRestores;
        $this->collUsersRestoresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsersRestore objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsersRestore objects.
     * @throws PropelException
     */
    public function countUsersRestores(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersRestoresPartial && !$this->isNew();
        if (null === $this->collUsersRestores || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsersRestores) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsersRestores());
            }

            $query = ChildUsersRestoreQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBybundle_UsersRestore_Users($this)
                ->count($con);
        }

        return count($this->collUsersRestores);
    }

    /**
     * Method called to associate a ChildUsersRestore object to this object
     * through the ChildUsersRestore foreign key attribute.
     *
     * @param  ChildUsersRestore $l ChildUsersRestore
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function addUsersRestore(ChildUsersRestore $l)
    {
        if ($this->collUsersRestores === null) {
            $this->initUsersRestores();
            $this->collUsersRestoresPartial = true;
        }

        if (!$this->collUsersRestores->contains($l)) {
            $this->doAddUsersRestore($l);

            if ($this->usersRestoresScheduledForDeletion and $this->usersRestoresScheduledForDeletion->contains($l)) {
                $this->usersRestoresScheduledForDeletion->remove($this->usersRestoresScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsersRestore $usersRestore The ChildUsersRestore object to add.
     */
    protected function doAddUsersRestore(ChildUsersRestore $usersRestore)
    {
        $this->collUsersRestores[]= $usersRestore;
        $usersRestore->setbundle_UsersRestore_Users($this);
    }

    /**
     * @param  ChildUsersRestore $usersRestore The ChildUsersRestore object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeUsersRestore(ChildUsersRestore $usersRestore)
    {
        if ($this->getUsersRestores()->contains($usersRestore)) {
            $pos = $this->collUsersRestores->search($usersRestore);
            $this->collUsersRestores->remove($pos);
            if (null === $this->usersRestoresScheduledForDeletion) {
                $this->usersRestoresScheduledForDeletion = clone $this->collUsersRestores;
                $this->usersRestoresScheduledForDeletion->clear();
            }
            $this->usersRestoresScheduledForDeletion[]= clone $usersRestore;
            $usersRestore->setbundle_UsersRestore_Users(null);
        }

        return $this;
    }

    /**
     * Clears out the collUsersFieldss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsersFieldss()
     */
    public function clearUsersFieldss()
    {
        $this->collUsersFieldss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsersFieldss collection loaded partially.
     */
    public function resetPartialUsersFieldss($v = true)
    {
        $this->collUsersFieldssPartial = $v;
    }

    /**
     * Initializes the collUsersFieldss collection.
     *
     * By default this just sets the collUsersFieldss collection to an empty array (like clearcollUsersFieldss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsersFieldss($overrideExisting = true)
    {
        if (null !== $this->collUsersFieldss && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsersFieldsTableMap::getTableMap()->getCollectionClassName();

        $this->collUsersFieldss = new $collectionClassName;
        $this->collUsersFieldss->setModel('\StayOut\DB\UsersFields');
    }

    /**
     * Gets an array of ChildUsersFields objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsersFields[] List of ChildUsersFields objects
     * @throws PropelException
     */
    public function getUsersFieldss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersFieldssPartial && !$this->isNew();
        if (null === $this->collUsersFieldss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUsersFieldss) {
                // return empty collection
                $this->initUsersFieldss();
            } else {
                $collUsersFieldss = ChildUsersFieldsQuery::create(null, $criteria)
                    ->filterBybundle_UsersFields_Users($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsersFieldssPartial && count($collUsersFieldss)) {
                        $this->initUsersFieldss(false);

                        foreach ($collUsersFieldss as $obj) {
                            if (false == $this->collUsersFieldss->contains($obj)) {
                                $this->collUsersFieldss->append($obj);
                            }
                        }

                        $this->collUsersFieldssPartial = true;
                    }

                    return $collUsersFieldss;
                }

                if ($partial && $this->collUsersFieldss) {
                    foreach ($this->collUsersFieldss as $obj) {
                        if ($obj->isNew()) {
                            $collUsersFieldss[] = $obj;
                        }
                    }
                }

                $this->collUsersFieldss = $collUsersFieldss;
                $this->collUsersFieldssPartial = false;
            }
        }

        return $this->collUsersFieldss;
    }

    /**
     * Sets a collection of ChildUsersFields objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usersFieldss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setUsersFieldss(Collection $usersFieldss, ConnectionInterface $con = null)
    {
        /** @var ChildUsersFields[] $usersFieldssToDelete */
        $usersFieldssToDelete = $this->getUsersFieldss(new Criteria(), $con)->diff($usersFieldss);


        $this->usersFieldssScheduledForDeletion = $usersFieldssToDelete;

        foreach ($usersFieldssToDelete as $usersFieldsRemoved) {
            $usersFieldsRemoved->setbundle_UsersFields_Users(null);
        }

        $this->collUsersFieldss = null;
        foreach ($usersFieldss as $usersFields) {
            $this->addUsersFields($usersFields);
        }

        $this->collUsersFieldss = $usersFieldss;
        $this->collUsersFieldssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsersFields objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsersFields objects.
     * @throws PropelException
     */
    public function countUsersFieldss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersFieldssPartial && !$this->isNew();
        if (null === $this->collUsersFieldss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsersFieldss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsersFieldss());
            }

            $query = ChildUsersFieldsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBybundle_UsersFields_Users($this)
                ->count($con);
        }

        return count($this->collUsersFieldss);
    }

    /**
     * Method called to associate a ChildUsersFields object to this object
     * through the ChildUsersFields foreign key attribute.
     *
     * @param  ChildUsersFields $l ChildUsersFields
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function addUsersFields(ChildUsersFields $l)
    {
        if ($this->collUsersFieldss === null) {
            $this->initUsersFieldss();
            $this->collUsersFieldssPartial = true;
        }

        if (!$this->collUsersFieldss->contains($l)) {
            $this->doAddUsersFields($l);

            if ($this->usersFieldssScheduledForDeletion and $this->usersFieldssScheduledForDeletion->contains($l)) {
                $this->usersFieldssScheduledForDeletion->remove($this->usersFieldssScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsersFields $usersFields The ChildUsersFields object to add.
     */
    protected function doAddUsersFields(ChildUsersFields $usersFields)
    {
        $this->collUsersFieldss[]= $usersFields;
        $usersFields->setbundle_UsersFields_Users($this);
    }

    /**
     * @param  ChildUsersFields $usersFields The ChildUsersFields object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeUsersFields(ChildUsersFields $usersFields)
    {
        if ($this->getUsersFieldss()->contains($usersFields)) {
            $pos = $this->collUsersFieldss->search($usersFields);
            $this->collUsersFieldss->remove($pos);
            if (null === $this->usersFieldssScheduledForDeletion) {
                $this->usersFieldssScheduledForDeletion = clone $this->collUsersFieldss;
                $this->usersFieldssScheduledForDeletion->clear();
            }
            $this->usersFieldssScheduledForDeletion[]= clone $usersFields;
            $usersFields->setbundle_UsersFields_Users(null);
        }

        return $this;
    }

    /**
     * Clears out the collUsersAccesses collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsersAccesses()
     */
    public function clearUsersAccesses()
    {
        $this->collUsersAccesses = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsersAccesses collection loaded partially.
     */
    public function resetPartialUsersAccesses($v = true)
    {
        $this->collUsersAccessesPartial = $v;
    }

    /**
     * Initializes the collUsersAccesses collection.
     *
     * By default this just sets the collUsersAccesses collection to an empty array (like clearcollUsersAccesses());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsersAccesses($overrideExisting = true)
    {
        if (null !== $this->collUsersAccesses && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsersAccessTableMap::getTableMap()->getCollectionClassName();

        $this->collUsersAccesses = new $collectionClassName;
        $this->collUsersAccesses->setModel('\StayOut\DB\UsersAccess');
    }

    /**
     * Gets an array of ChildUsersAccess objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsersAccess[] List of ChildUsersAccess objects
     * @throws PropelException
     */
    public function getUsersAccesses(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersAccessesPartial && !$this->isNew();
        if (null === $this->collUsersAccesses || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUsersAccesses) {
                // return empty collection
                $this->initUsersAccesses();
            } else {
                $collUsersAccesses = ChildUsersAccessQuery::create(null, $criteria)
                    ->filterBybundle_UsersAccess_Users($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsersAccessesPartial && count($collUsersAccesses)) {
                        $this->initUsersAccesses(false);

                        foreach ($collUsersAccesses as $obj) {
                            if (false == $this->collUsersAccesses->contains($obj)) {
                                $this->collUsersAccesses->append($obj);
                            }
                        }

                        $this->collUsersAccessesPartial = true;
                    }

                    return $collUsersAccesses;
                }

                if ($partial && $this->collUsersAccesses) {
                    foreach ($this->collUsersAccesses as $obj) {
                        if ($obj->isNew()) {
                            $collUsersAccesses[] = $obj;
                        }
                    }
                }

                $this->collUsersAccesses = $collUsersAccesses;
                $this->collUsersAccessesPartial = false;
            }
        }

        return $this->collUsersAccesses;
    }

    /**
     * Sets a collection of ChildUsersAccess objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usersAccesses A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function setUsersAccesses(Collection $usersAccesses, ConnectionInterface $con = null)
    {
        /** @var ChildUsersAccess[] $usersAccessesToDelete */
        $usersAccessesToDelete = $this->getUsersAccesses(new Criteria(), $con)->diff($usersAccesses);


        $this->usersAccessesScheduledForDeletion = $usersAccessesToDelete;

        foreach ($usersAccessesToDelete as $usersAccessRemoved) {
            $usersAccessRemoved->setbundle_UsersAccess_Users(null);
        }

        $this->collUsersAccesses = null;
        foreach ($usersAccesses as $usersAccess) {
            $this->addUsersAccess($usersAccess);
        }

        $this->collUsersAccesses = $usersAccesses;
        $this->collUsersAccessesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsersAccess objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsersAccess objects.
     * @throws PropelException
     */
    public function countUsersAccesses(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersAccessesPartial && !$this->isNew();
        if (null === $this->collUsersAccesses || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsersAccesses) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsersAccesses());
            }

            $query = ChildUsersAccessQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBybundle_UsersAccess_Users($this)
                ->count($con);
        }

        return count($this->collUsersAccesses);
    }

    /**
     * Method called to associate a ChildUsersAccess object to this object
     * through the ChildUsersAccess foreign key attribute.
     *
     * @param  ChildUsersAccess $l ChildUsersAccess
     * @return $this|\StayOut\DB\Users The current object (for fluent API support)
     */
    public function addUsersAccess(ChildUsersAccess $l)
    {
        if ($this->collUsersAccesses === null) {
            $this->initUsersAccesses();
            $this->collUsersAccessesPartial = true;
        }

        if (!$this->collUsersAccesses->contains($l)) {
            $this->doAddUsersAccess($l);

            if ($this->usersAccessesScheduledForDeletion and $this->usersAccessesScheduledForDeletion->contains($l)) {
                $this->usersAccessesScheduledForDeletion->remove($this->usersAccessesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsersAccess $usersAccess The ChildUsersAccess object to add.
     */
    protected function doAddUsersAccess(ChildUsersAccess $usersAccess)
    {
        $this->collUsersAccesses[]= $usersAccess;
        $usersAccess->setbundle_UsersAccess_Users($this);
    }

    /**
     * @param  ChildUsersAccess $usersAccess The ChildUsersAccess object to remove.
     * @return $this|ChildUsers The current object (for fluent API support)
     */
    public function removeUsersAccess(ChildUsersAccess $usersAccess)
    {
        if ($this->getUsersAccesses()->contains($usersAccess)) {
            $pos = $this->collUsersAccesses->search($usersAccess);
            $this->collUsersAccesses->remove($pos);
            if (null === $this->usersAccessesScheduledForDeletion) {
                $this->usersAccessesScheduledForDeletion = clone $this->collUsersAccesses;
                $this->usersAccessesScheduledForDeletion->clear();
            }
            $this->usersAccessesScheduledForDeletion[]= clone $usersAccess;
            $usersAccess->setbundle_UsersAccess_Users(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->hash_id = null;
        $this->email = null;
        $this->login = null;
        $this->password = null;
        $this->is_activated = null;
        $this->activate_code = null;
        $this->is_deleted = null;
        $this->auth_vk = null;
        $this->auth_fb = null;
        $this->date_create = null;
        $this->date_update = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUsersTokenss) {
                foreach ($this->collUsersTokenss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsersRestores) {
                foreach ($this->collUsersRestores as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsersFieldss) {
                foreach ($this->collUsersFieldss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsersAccesses) {
                foreach ($this->collUsersAccesses as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUsersTokenss = null;
        $this->collUsersRestores = null;
        $this->collUsersFieldss = null;
        $this->collUsersAccesses = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UsersTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
