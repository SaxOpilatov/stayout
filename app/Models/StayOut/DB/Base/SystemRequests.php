<?php

namespace StayOut\DB\Base;

use \DateTime;
use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;
use StayOut\DB\SystemErrors as ChildSystemErrors;
use StayOut\DB\SystemErrorsQuery as ChildSystemErrorsQuery;
use StayOut\DB\SystemRequests as ChildSystemRequests;
use StayOut\DB\SystemRequestsQuery as ChildSystemRequestsQuery;
use StayOut\DB\Map\SystemErrorsTableMap;
use StayOut\DB\Map\SystemRequestsTableMap;

/**
 * Base class that represents a row from the 'system_requests' table.
 *
 *
 *
 * @package    propel.generator.StayOut.DB.Base
 */
abstract class SystemRequests implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\StayOut\\DB\\Map\\SystemRequestsTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * The value for the method field.
     *
     * @var        string
     */
    protected $method;

    /**
     * The value for the headers field.
     *
     * @var        string
     */
    protected $headers;

    /**
     * The value for the gets field.
     *
     * @var        string
     */
    protected $gets;

    /**
     * The value for the posts field.
     *
     * @var        string
     */
    protected $posts;

    /**
     * The value for the inputs field.
     *
     * @var        string
     */
    protected $inputs;

    /**
     * The value for the ip field.
     *
     * @var        string
     */
    protected $ip;

    /**
     * The value for the has_errors field.
     *
     * @var        boolean
     */
    protected $has_errors;

    /**
     * The value for the date_create field.
     *
     * @var        DateTime
     */
    protected $date_create;

    /**
     * @var        ObjectCollection|ChildSystemErrors[] Collection to store aggregation of ChildSystemErrors objects.
     */
    protected $collSystemErrorss;
    protected $collSystemErrorssPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSystemErrors[]
     */
    protected $systemErrorssScheduledForDeletion = null;

    /**
     * Initializes internal state of StayOut\DB\Base\SystemRequests object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SystemRequests</code> instance.  If
     * <code>obj</code> is an instance of <code>SystemRequests</code>, delegates to
     * <code>equals(SystemRequests)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|SystemRequests The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [method] column value.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Get the [headers] column value.
     *
     * @return string
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Get the [gets] column value.
     *
     * @return string
     */
    public function getGets()
    {
        return $this->gets;
    }

    /**
     * Get the [posts] column value.
     *
     * @return string
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Get the [inputs] column value.
     *
     * @return string
     */
    public function getInputs()
    {
        return $this->inputs;
    }

    /**
     * Get the [ip] column value.
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Get the [has_errors] column value.
     *
     * @return boolean
     */
    public function getHasErrors()
    {
        return $this->has_errors;
    }

    /**
     * Get the [has_errors] column value.
     *
     * @return boolean
     */
    public function hasErrors()
    {
        return $this->getHasErrors();
    }

    /**
     * Get the [optionally formatted] temporal [date_create] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreate($format = NULL)
    {
        if ($format === null) {
            return $this->date_create;
        } else {
            return $this->date_create instanceof \DateTimeInterface ? $this->date_create->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [method] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setMethod($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->method !== $v) {
            $this->method = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_METHOD] = true;
        }

        return $this;
    } // setMethod()

    /**
     * Set the value of [headers] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setHeaders($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->headers !== $v) {
            $this->headers = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_HEADERS] = true;
        }

        return $this;
    } // setHeaders()

    /**
     * Set the value of [gets] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setGets($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gets !== $v) {
            $this->gets = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_GETS] = true;
        }

        return $this;
    } // setGets()

    /**
     * Set the value of [posts] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setPosts($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->posts !== $v) {
            $this->posts = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_POSTS] = true;
        }

        return $this;
    } // setPosts()

    /**
     * Set the value of [inputs] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setInputs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->inputs !== $v) {
            $this->inputs = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_INPUTS] = true;
        }

        return $this;
    } // setInputs()

    /**
     * Set the value of [ip] column.
     *
     * @param string $v new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setIp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ip !== $v) {
            $this->ip = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_IP] = true;
        }

        return $this;
    } // setIp()

    /**
     * Sets the value of the [has_errors] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setHasErrors($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_errors !== $v) {
            $this->has_errors = $v;
            $this->modifiedColumns[SystemRequestsTableMap::COL_HAS_ERRORS] = true;
        }

        return $this;
    } // setHasErrors()

    /**
     * Sets the value of [date_create] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function setDateCreate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_create !== null || $dt !== null) {
            if ($this->date_create === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_create->format("Y-m-d H:i:s.u")) {
                $this->date_create = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SystemRequestsTableMap::COL_DATE_CREATE] = true;
            }
        } // if either are not null

        return $this;
    } // setDateCreate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SystemRequestsTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SystemRequestsTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SystemRequestsTableMap::translateFieldName('Method', TableMap::TYPE_PHPNAME, $indexType)];
            $this->method = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SystemRequestsTableMap::translateFieldName('Headers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->headers = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SystemRequestsTableMap::translateFieldName('Gets', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gets = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SystemRequestsTableMap::translateFieldName('Posts', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posts = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SystemRequestsTableMap::translateFieldName('Inputs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->inputs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SystemRequestsTableMap::translateFieldName('Ip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ip = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SystemRequestsTableMap::translateFieldName('HasErrors', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_errors = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SystemRequestsTableMap::translateFieldName('DateCreate', TableMap::TYPE_PHPNAME, $indexType)];
            $this->date_create = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = SystemRequestsTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\StayOut\\DB\\SystemRequests'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SystemRequestsTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSystemRequestsQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collSystemErrorss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SystemRequests::setDeleted()
     * @see SystemRequests::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemRequestsTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSystemRequestsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemRequestsTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SystemRequestsTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->systemErrorssScheduledForDeletion !== null) {
                if (!$this->systemErrorssScheduledForDeletion->isEmpty()) {
                    \StayOut\DB\SystemErrorsQuery::create()
                        ->filterByPrimaryKeys($this->systemErrorssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->systemErrorssScheduledForDeletion = null;
                }
            }

            if ($this->collSystemErrorss !== null) {
                foreach ($this->collSystemErrorss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SystemRequestsTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SystemRequestsTableMap::COL_ID . ')');
        }
        if (null === $this->id) {
            try {
                $dataFetcher = $con->query("SELECT nextval('system_requests_id_seq')");
                $this->id = (string) $dataFetcher->fetchColumn();
            } catch (Exception $e) {
                throw new PropelException('Unable to get sequence id.', 0, $e);
            }
        }


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SystemRequestsTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'url';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_METHOD)) {
            $modifiedColumns[':p' . $index++]  = 'method';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_HEADERS)) {
            $modifiedColumns[':p' . $index++]  = 'headers';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_GETS)) {
            $modifiedColumns[':p' . $index++]  = 'gets';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_POSTS)) {
            $modifiedColumns[':p' . $index++]  = 'posts';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_INPUTS)) {
            $modifiedColumns[':p' . $index++]  = 'inputs';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_IP)) {
            $modifiedColumns[':p' . $index++]  = 'ip';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_HAS_ERRORS)) {
            $modifiedColumns[':p' . $index++]  = 'has_errors';
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_DATE_CREATE)) {
            $modifiedColumns[':p' . $index++]  = 'date_create';
        }

        $sql = sprintf(
            'INSERT INTO system_requests (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'url':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case 'method':
                        $stmt->bindValue($identifier, $this->method, PDO::PARAM_STR);
                        break;
                    case 'headers':
                        $stmt->bindValue($identifier, $this->headers, PDO::PARAM_STR);
                        break;
                    case 'gets':
                        $stmt->bindValue($identifier, $this->gets, PDO::PARAM_STR);
                        break;
                    case 'posts':
                        $stmt->bindValue($identifier, $this->posts, PDO::PARAM_STR);
                        break;
                    case 'inputs':
                        $stmt->bindValue($identifier, $this->inputs, PDO::PARAM_STR);
                        break;
                    case 'ip':
                        $stmt->bindValue($identifier, $this->ip, PDO::PARAM_STR);
                        break;
                    case 'has_errors':
                        $stmt->bindValue($identifier, $this->has_errors, PDO::PARAM_BOOL);
                        break;
                    case 'date_create':
                        $stmt->bindValue($identifier, $this->date_create ? $this->date_create->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SystemRequestsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUrl();
                break;
            case 2:
                return $this->getMethod();
                break;
            case 3:
                return $this->getHeaders();
                break;
            case 4:
                return $this->getGets();
                break;
            case 5:
                return $this->getPosts();
                break;
            case 6:
                return $this->getInputs();
                break;
            case 7:
                return $this->getIp();
                break;
            case 8:
                return $this->getHasErrors();
                break;
            case 9:
                return $this->getDateCreate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SystemRequests'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SystemRequests'][$this->hashCode()] = true;
        $keys = SystemRequestsTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUrl(),
            $keys[2] => $this->getMethod(),
            $keys[3] => $this->getHeaders(),
            $keys[4] => $this->getGets(),
            $keys[5] => $this->getPosts(),
            $keys[6] => $this->getInputs(),
            $keys[7] => $this->getIp(),
            $keys[8] => $this->getHasErrors(),
            $keys[9] => $this->getDateCreate(),
        );
        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collSystemErrorss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'systemErrorss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'system_errorss';
                        break;
                    default:
                        $key = 'SystemErrorss';
                }

                $result[$key] = $this->collSystemErrorss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\StayOut\DB\SystemRequests
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SystemRequestsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\StayOut\DB\SystemRequests
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUrl($value);
                break;
            case 2:
                $this->setMethod($value);
                break;
            case 3:
                $this->setHeaders($value);
                break;
            case 4:
                $this->setGets($value);
                break;
            case 5:
                $this->setPosts($value);
                break;
            case 6:
                $this->setInputs($value);
                break;
            case 7:
                $this->setIp($value);
                break;
            case 8:
                $this->setHasErrors($value);
                break;
            case 9:
                $this->setDateCreate($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SystemRequestsTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUrl($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMethod($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setHeaders($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setGets($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPosts($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setInputs($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIp($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setHasErrors($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDateCreate($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\StayOut\DB\SystemRequests The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SystemRequestsTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SystemRequestsTableMap::COL_ID)) {
            $criteria->add(SystemRequestsTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_URL)) {
            $criteria->add(SystemRequestsTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_METHOD)) {
            $criteria->add(SystemRequestsTableMap::COL_METHOD, $this->method);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_HEADERS)) {
            $criteria->add(SystemRequestsTableMap::COL_HEADERS, $this->headers);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_GETS)) {
            $criteria->add(SystemRequestsTableMap::COL_GETS, $this->gets);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_POSTS)) {
            $criteria->add(SystemRequestsTableMap::COL_POSTS, $this->posts);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_INPUTS)) {
            $criteria->add(SystemRequestsTableMap::COL_INPUTS, $this->inputs);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_IP)) {
            $criteria->add(SystemRequestsTableMap::COL_IP, $this->ip);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_HAS_ERRORS)) {
            $criteria->add(SystemRequestsTableMap::COL_HAS_ERRORS, $this->has_errors);
        }
        if ($this->isColumnModified(SystemRequestsTableMap::COL_DATE_CREATE)) {
            $criteria->add(SystemRequestsTableMap::COL_DATE_CREATE, $this->date_create);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSystemRequestsQuery::create();
        $criteria->add(SystemRequestsTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \StayOut\DB\SystemRequests (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUrl($this->getUrl());
        $copyObj->setMethod($this->getMethod());
        $copyObj->setHeaders($this->getHeaders());
        $copyObj->setGets($this->getGets());
        $copyObj->setPosts($this->getPosts());
        $copyObj->setInputs($this->getInputs());
        $copyObj->setIp($this->getIp());
        $copyObj->setHasErrors($this->getHasErrors());
        $copyObj->setDateCreate($this->getDateCreate());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSystemErrorss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSystemErrors($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \StayOut\DB\SystemRequests Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SystemErrors' == $relationName) {
            $this->initSystemErrorss();
            return;
        }
    }

    /**
     * Clears out the collSystemErrorss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSystemErrorss()
     */
    public function clearSystemErrorss()
    {
        $this->collSystemErrorss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSystemErrorss collection loaded partially.
     */
    public function resetPartialSystemErrorss($v = true)
    {
        $this->collSystemErrorssPartial = $v;
    }

    /**
     * Initializes the collSystemErrorss collection.
     *
     * By default this just sets the collSystemErrorss collection to an empty array (like clearcollSystemErrorss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSystemErrorss($overrideExisting = true)
    {
        if (null !== $this->collSystemErrorss && !$overrideExisting) {
            return;
        }

        $collectionClassName = SystemErrorsTableMap::getTableMap()->getCollectionClassName();

        $this->collSystemErrorss = new $collectionClassName;
        $this->collSystemErrorss->setModel('\StayOut\DB\SystemErrors');
    }

    /**
     * Gets an array of ChildSystemErrors objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSystemRequests is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSystemErrors[] List of ChildSystemErrors objects
     * @throws PropelException
     */
    public function getSystemErrorss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSystemErrorssPartial && !$this->isNew();
        if (null === $this->collSystemErrorss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSystemErrorss) {
                // return empty collection
                $this->initSystemErrorss();
            } else {
                $collSystemErrorss = ChildSystemErrorsQuery::create(null, $criteria)
                    ->filterBybundle_SystemErrors_SystemRequests($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSystemErrorssPartial && count($collSystemErrorss)) {
                        $this->initSystemErrorss(false);

                        foreach ($collSystemErrorss as $obj) {
                            if (false == $this->collSystemErrorss->contains($obj)) {
                                $this->collSystemErrorss->append($obj);
                            }
                        }

                        $this->collSystemErrorssPartial = true;
                    }

                    return $collSystemErrorss;
                }

                if ($partial && $this->collSystemErrorss) {
                    foreach ($this->collSystemErrorss as $obj) {
                        if ($obj->isNew()) {
                            $collSystemErrorss[] = $obj;
                        }
                    }
                }

                $this->collSystemErrorss = $collSystemErrorss;
                $this->collSystemErrorssPartial = false;
            }
        }

        return $this->collSystemErrorss;
    }

    /**
     * Sets a collection of ChildSystemErrors objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $systemErrorss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSystemRequests The current object (for fluent API support)
     */
    public function setSystemErrorss(Collection $systemErrorss, ConnectionInterface $con = null)
    {
        /** @var ChildSystemErrors[] $systemErrorssToDelete */
        $systemErrorssToDelete = $this->getSystemErrorss(new Criteria(), $con)->diff($systemErrorss);


        $this->systemErrorssScheduledForDeletion = $systemErrorssToDelete;

        foreach ($systemErrorssToDelete as $systemErrorsRemoved) {
            $systemErrorsRemoved->setbundle_SystemErrors_SystemRequests(null);
        }

        $this->collSystemErrorss = null;
        foreach ($systemErrorss as $systemErrors) {
            $this->addSystemErrors($systemErrors);
        }

        $this->collSystemErrorss = $systemErrorss;
        $this->collSystemErrorssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SystemErrors objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SystemErrors objects.
     * @throws PropelException
     */
    public function countSystemErrorss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSystemErrorssPartial && !$this->isNew();
        if (null === $this->collSystemErrorss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSystemErrorss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSystemErrorss());
            }

            $query = ChildSystemErrorsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBybundle_SystemErrors_SystemRequests($this)
                ->count($con);
        }

        return count($this->collSystemErrorss);
    }

    /**
     * Method called to associate a ChildSystemErrors object to this object
     * through the ChildSystemErrors foreign key attribute.
     *
     * @param  ChildSystemErrors $l ChildSystemErrors
     * @return $this|\StayOut\DB\SystemRequests The current object (for fluent API support)
     */
    public function addSystemErrors(ChildSystemErrors $l)
    {
        if ($this->collSystemErrorss === null) {
            $this->initSystemErrorss();
            $this->collSystemErrorssPartial = true;
        }

        if (!$this->collSystemErrorss->contains($l)) {
            $this->doAddSystemErrors($l);

            if ($this->systemErrorssScheduledForDeletion and $this->systemErrorssScheduledForDeletion->contains($l)) {
                $this->systemErrorssScheduledForDeletion->remove($this->systemErrorssScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSystemErrors $systemErrors The ChildSystemErrors object to add.
     */
    protected function doAddSystemErrors(ChildSystemErrors $systemErrors)
    {
        $this->collSystemErrorss[]= $systemErrors;
        $systemErrors->setbundle_SystemErrors_SystemRequests($this);
    }

    /**
     * @param  ChildSystemErrors $systemErrors The ChildSystemErrors object to remove.
     * @return $this|ChildSystemRequests The current object (for fluent API support)
     */
    public function removeSystemErrors(ChildSystemErrors $systemErrors)
    {
        if ($this->getSystemErrorss()->contains($systemErrors)) {
            $pos = $this->collSystemErrorss->search($systemErrors);
            $this->collSystemErrorss->remove($pos);
            if (null === $this->systemErrorssScheduledForDeletion) {
                $this->systemErrorssScheduledForDeletion = clone $this->collSystemErrorss;
                $this->systemErrorssScheduledForDeletion->clear();
            }
            $this->systemErrorssScheduledForDeletion[]= clone $systemErrors;
            $systemErrors->setbundle_SystemErrors_SystemRequests(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->url = null;
        $this->method = null;
        $this->headers = null;
        $this->gets = null;
        $this->posts = null;
        $this->inputs = null;
        $this->ip = null;
        $this->has_errors = null;
        $this->date_create = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSystemErrorss) {
                foreach ($this->collSystemErrorss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSystemErrorss = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SystemRequestsTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
