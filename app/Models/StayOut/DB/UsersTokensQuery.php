<?php

namespace StayOut\DB;

use StayOut\DB\Base\UsersTokensQuery as BaseUsersTokensQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'users_tokens' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UsersTokensQuery extends BaseUsersTokensQuery
{

}
