<?php

namespace StayOut\DB;

use StayOut\DB\Base\DataCountriesQuery as BaseDataCountriesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'data_countries' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DataCountriesQuery extends BaseDataCountriesQuery
{

}
