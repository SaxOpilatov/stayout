<?php

namespace StayOut\DB;

use StayOut\DB\Base\EmailTemplates as BaseEmailTemplates;

/**
 * Skeleton subclass for representing a row from the 'email_templates' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class EmailTemplates extends BaseEmailTemplates
{

}
