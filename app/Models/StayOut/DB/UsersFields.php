<?php

namespace StayOut\DB;

use StayOut\DB\Base\UsersFields as BaseUsersFields;

/**
 * Skeleton subclass for representing a row from the 'users_fields' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UsersFields extends BaseUsersFields
{

}
