<?php

namespace StayOut\DB\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use StayOut\DB\UsersRestore;
use StayOut\DB\UsersRestoreQuery;


/**
 * This class defines the structure of the 'users_restore' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UsersRestoreTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'StayOut.DB.Map.UsersRestoreTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'users_restore';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\StayOut\\DB\\UsersRestore';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'StayOut.DB.UsersRestore';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'users_restore.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'users_restore.user_id';

    /**
     * the column name for the restore_hash field
     */
    const COL_RESTORE_HASH = 'users_restore.restore_hash';

    /**
     * the column name for the restore_code field
     */
    const COL_RESTORE_CODE = 'users_restore.restore_code';

    /**
     * the column name for the is_used field
     */
    const COL_IS_USED = 'users_restore.is_used';

    /**
     * the column name for the date_create field
     */
    const COL_DATE_CREATE = 'users_restore.date_create';

    /**
     * the column name for the date_update field
     */
    const COL_DATE_UPDATE = 'users_restore.date_update';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'RestoreHash', 'RestoreCode', 'IsUsed', 'DateCreate', 'DateUpdate', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'restoreHash', 'restoreCode', 'isUsed', 'dateCreate', 'dateUpdate', ),
        self::TYPE_COLNAME       => array(UsersRestoreTableMap::COL_ID, UsersRestoreTableMap::COL_USER_ID, UsersRestoreTableMap::COL_RESTORE_HASH, UsersRestoreTableMap::COL_RESTORE_CODE, UsersRestoreTableMap::COL_IS_USED, UsersRestoreTableMap::COL_DATE_CREATE, UsersRestoreTableMap::COL_DATE_UPDATE, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'restore_hash', 'restore_code', 'is_used', 'date_create', 'date_update', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'RestoreHash' => 2, 'RestoreCode' => 3, 'IsUsed' => 4, 'DateCreate' => 5, 'DateUpdate' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'restoreHash' => 2, 'restoreCode' => 3, 'isUsed' => 4, 'dateCreate' => 5, 'dateUpdate' => 6, ),
        self::TYPE_COLNAME       => array(UsersRestoreTableMap::COL_ID => 0, UsersRestoreTableMap::COL_USER_ID => 1, UsersRestoreTableMap::COL_RESTORE_HASH => 2, UsersRestoreTableMap::COL_RESTORE_CODE => 3, UsersRestoreTableMap::COL_IS_USED => 4, UsersRestoreTableMap::COL_DATE_CREATE => 5, UsersRestoreTableMap::COL_DATE_UPDATE => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'restore_hash' => 2, 'restore_code' => 3, 'is_used' => 4, 'date_create' => 5, 'date_update' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('users_restore');
        $this->setPhpName('UsersRestore');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\StayOut\\DB\\UsersRestore');
        $this->setPackage('StayOut.DB');
        $this->setUseIdGenerator(true);
        $this->setPrimaryKeyMethodInfo('users_restore_id_seq');
        // columns
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'BIGINT', 'users', 'id', true, null, null);
        $this->addColumn('restore_hash', 'RestoreHash', 'VARCHAR', true, 255, null);
        $this->addColumn('restore_code', 'RestoreCode', 'BIGINT', true, null, null);
        $this->addColumn('is_used', 'IsUsed', 'BOOLEAN', false, null, null);
        $this->addColumn('date_create', 'DateCreate', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_update', 'DateUpdate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('bundle_UsersRestore_Users', '\\StayOut\\DB\\Users', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UsersRestoreTableMap::CLASS_DEFAULT : UsersRestoreTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UsersRestore object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UsersRestoreTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UsersRestoreTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UsersRestoreTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UsersRestoreTableMap::OM_CLASS;
            /** @var UsersRestore $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UsersRestoreTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UsersRestoreTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UsersRestoreTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UsersRestore $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UsersRestoreTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UsersRestoreTableMap::COL_ID);
            $criteria->addSelectColumn(UsersRestoreTableMap::COL_USER_ID);
            $criteria->addSelectColumn(UsersRestoreTableMap::COL_RESTORE_HASH);
            $criteria->addSelectColumn(UsersRestoreTableMap::COL_RESTORE_CODE);
            $criteria->addSelectColumn(UsersRestoreTableMap::COL_IS_USED);
            $criteria->addSelectColumn(UsersRestoreTableMap::COL_DATE_CREATE);
            $criteria->addSelectColumn(UsersRestoreTableMap::COL_DATE_UPDATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.restore_hash');
            $criteria->addSelectColumn($alias . '.restore_code');
            $criteria->addSelectColumn($alias . '.is_used');
            $criteria->addSelectColumn($alias . '.date_create');
            $criteria->addSelectColumn($alias . '.date_update');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UsersRestoreTableMap::DATABASE_NAME)->getTable(UsersRestoreTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UsersRestoreTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UsersRestoreTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UsersRestoreTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UsersRestore or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UsersRestore object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersRestoreTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \StayOut\DB\UsersRestore) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UsersRestoreTableMap::DATABASE_NAME);
            $criteria->add(UsersRestoreTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UsersRestoreQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UsersRestoreTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UsersRestoreTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the users_restore table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UsersRestoreQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UsersRestore or Criteria object.
     *
     * @param mixed               $criteria Criteria or UsersRestore object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsersRestoreTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UsersRestore object
        }

        if ($criteria->containsKey(UsersRestoreTableMap::COL_ID) && $criteria->keyContainsValue(UsersRestoreTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UsersRestoreTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UsersRestoreQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UsersRestoreTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UsersRestoreTableMap::buildTableMap();
