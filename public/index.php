<?php

use StayOut\Bootstrap as App;

require_once __DIR__ . '/../system/bootstrap.php';

$app = new App();
$app->run();
